# TP4 : Vers un réseau d'entreprise

On va utiliser GNS3 dans ce TP pour se rapprocher d'un cas réel. On va focus sur l'aspect routing/switching, avec du matériel Cisco. On va aussi mettre en place des VLANs.

![best memes from cisco doc](./pics/the-best-memes-come-from-cisco-documentation.jpg)

# Sommaire

- [TP4 : Vers un réseau d'entreprise](#tp4--vers-un-réseau-dentreprise)
- [Sommaire](#sommaire)
- [0. Prérequis](#0-prérequis)
  - [Checklist VM Linux](#checklist-vm-linux)
- [I. Dumb switch](#i-dumb-switch)
  - [1. Topologie 1](#1-topologie-1)
  - [2. Adressage topologie 1](#2-adressage-topologie-1)
  - [3. Setup topologie 1](#3-setup-topologie-1)
- [II. VLAN](#ii-vlan)
  - [1. Topologie 2](#1-topologie-2)
  - [2. Adressage topologie 2](#2-adressage-topologie-2)
    - [3. Setup topologie 2](#3-setup-topologie-2)
- [III. Routing](#iii-routing)
  - [1. Topologie 3](#1-topologie-3)
  - [2. Adressage topologie 3](#2-adressage-topologie-3)
  - [3. Setup topologie 3](#3-setup-topologie-3)
- [IV. NAT](#iv-nat)
  - [1. Topologie 4](#1-topologie-4)
  - [2. Adressage topologie 4](#2-adressage-topologie-4)
  - [3. Setup topologie 4](#3-setup-topologie-4)

# 0. Prérequis

➜ Les clients seront soit :

- VMs Rocky Linux
- VPCS
  - c'est un truc de GNS pour simuler un client du réseau
  - quand on veut juste un truc capable de faire des pings et rien de plus, c'est parfait
  - ça consomme R en ressources

> Faites bien attention aux logos des machines sur les schémas, et vous verrez clairement quand il faut un VPCS ou une VM.

➜ Les switches Cisco des vIOL2

➜ Les routeurs Cisco des c3640

➜ **Vous ne créerez aucune machine virtuelle au début. Vous les créerez au fur et à mesure que le TP vous le demande.** A chaque fois qu'une nouvelle machine devra être créée, vous trouverez l'emoji 🖥️ avec son nom.

## Checklist VM Linux

A chaque machine déployée, vous **DEVREZ** vérifier la 📝**checklist**📝 :

- [x] IP locale, statique ou dynamique
- [x] hostname défini
- [x] firewall actif, qui ne laisse passer que le strict nécessaire
- [x] on force une host-only, juste pour pouvoir SSH
- [x] SSH fonctionnel
- [x] résolution de nom
  - vers internet, quand vous aurez le routeur en place

**Les éléments de la 📝checklist📝 sont STRICTEMENT OBLIGATOIRES à réaliser mais ne doivent PAS figurer dans le rendu.**

# I. Dumb switch

## 1. Topologie 1

![Topologie 1](./pics/topo1.png)

## 2. Adressage topologie 1

| Node  | IP            |
|-------|---------------|
| `pc1` | `10.1.1.1/24` |
| `pc2` | `10.1.1.2/24` |

## 3. Setup topologie 1

🌞 **Commençons simple**

- définissez les IPs statiques sur les deux VPCS  
Pour le Pc1
```
PC1> ip 10.1.1.1
Checking for duplicate address...
PC1 : 10.1.1.1 255.255.255.0

PC1> save
Saving startup configuration to startup.vpc
.  done
```

Pour le Pc2
```
PC2> ip 10.1.1.2/24
Checking for duplicate address...
PC2 : 10.1.1.2 255.255.255.0

PC2> save
Saving startup configuration to startup.vpc
.  done
```


- `ping` un VPCS depuis l'autre
```PC1> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=3.888 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=4.822 ms
84 bytes from 10.1.1.2 icmp_seq=3 ttl=64 time=10.101 ms
84 bytes from 10.1.1.2 icmp_seq=4 ttl=64 time=3.934 ms
84 bytes from 10.1.1.2 icmp_seq=5 ttl=64 time=4.495 ms
```


> Jusque là, ça devrait aller. Noter qu'on a fait aucune conf sur le switch. Tant qu'on ne fait rien, c'est une bête multiprise.

# II. VLAN

**Le but dans cette partie va être de tester un peu les *VLANs*.**

On va rajouter **un troisième client** qui, bien que dans le même réseau, sera **isolé des autres grâce aux *VLANs***.

**Les *VLANs* sont une configuration à effectuer sur les *switches*.** C'est les *switches* qui effectuent le blocage.

Le principe est simple :

- déclaration du VLAN sur tous les switches
  - un VLAN a forcément un ID (un entier)
  - bonne pratique, on lui met un nom
- sur chaque switch, on définit le VLAN associé à chaque port
  - genre "sur le port 35, c'est un client du VLAN 20 qui est branché"

![VLAN FOR EVERYONE](./pics/get_a_vlan.jpg)

## 1. Topologie 2

![Topologie 2](./pics/topo2.png)

## 2. Adressage topologie 2

| Node  | IP            | VLAN |
|-------|---------------|------|
| `pc1` | `10.1.1.1/24` | 10   |
| `pc2` | `10.1.1.2/24` | 10   |
| `pc3` | `10.1.1.3/24` | 20   |

### 3. Setup topologie 2

🌞 **Adressage**

- définissez les IPs statiques sur tous les VPCS  

Pour le Pc3
```
PC3> ip 10.1.1.3/24
Checking for duplicate address...
PC3 : 10.1.1.3 255.255.255.0
```

- vérifiez avec des `ping` que tout le monde se ping

Ping de PC1 depuis PC2
```
PC2> ping 10.1.1.1

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=43.072 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=32.209 ms
84 bytes from 10.1.1.1 icmp_seq=3 ttl=64 time=31.759 ms
84 bytes from 10.1.1.1 icmp_seq=4 ttl=64 time=42.151 ms
84 bytes from 10.1.1.1 icmp_seq=5 ttl=64 time=37.073 ms
```

Ping de PC1 depuis PC3
```
PC3> ping 10.1.1.1

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=26.279 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=35.377 ms
84 bytes from 10.1.1.1 icmp_seq=3 ttl=64 time=34.161 ms
84 bytes from 10.1.1.1 icmp_seq=4 ttl=64 time=34.328 ms
84 bytes from 10.1.1.1 icmp_seq=5 ttl=64 time=39.594 ms
```

Ping de PC2 depuis PC3
```
PC3> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=27.513 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=48.557 ms
84 bytes from 10.1.1.2 icmp_seq=3 ttl=64 time=40.671 ms
84 bytes from 10.1.1.2 icmp_seq=4 ttl=64 time=36.475 ms
84 bytes from 10.1.1.2 icmp_seq=5 ttl=64 time=39.950 ms
```

🌞 **Configuration des VLANs**

- référez-vous [à la section VLAN du mémo Cisco](../../cours/memo/memo_cisco.md#8-vlan)
- déclaration des VLANs sur le switch `sw1`
- ajout des ports du switches dans le bon VLAN (voir [le tableau d'adressage de la topo 2 juste au dessus](#2-adressage-topologie-2))
  - ici, tous les ports sont en mode *access* : ils pointent vers des clients du réseau

```
Switch#conf t
Enter configuration commands, one per line.  End with CNTL/Z.

Switch(config)#vlan 10
Switch(config-vlan)#name pc1etpc2
Switch(config-vlan)#exit

Switch(config)#vlan 20
Switch(config-vlan)#name pc3
Switch(config-vlan)#exit

Switch(config)#interface Gi0/0
Switch(config-if)#switch mode access
Switch(config-if)#switch access vlan 10
Switch(config-if)#exit

Switch(config)#interface Gi0/1
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 10
Switch(config-if)#exit

Switch(config)#interface Gi0/2
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 20
Switch(config-if)#exit


Switch#show vlan br
VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Gi0/3, Gi1/0, Gi1/1, Gi1/2
                                                Gi1/3, Gi2/0, Gi2/1, Gi2/2
                                                Gi2/3, Gi3/0, Gi3/1, Gi3/2
                                                Gi3/3
10   pc1etpc2                         active    Gi0/0, Gi0/1
20   pc3                              active    Gi0/2
1002 fddi-default                     act/unsup
1003 token-ring-default               act/unsup
1004 fddinet-default                  act/unsup
1005 trnet-default                    act/unsup
```

🌞 **Vérif**

- `pc1` et `pc2` doivent toujours pouvoir se ping
```
PC1> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=27.857 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=16.687 ms
84 bytes from 10.1.1.2 icmp_seq=3 ttl=64 time=16.954 ms
84 bytes from 10.1.1.2 icmp_seq=4 ttl=64 time=41.388 ms
84 bytes from 10.1.1.2 icmp_seq=5 ttl=64 time=16.923 ms
```

- `pc3` ne ping plus personne
```

PC3> ping 10.1.1.1
host (10.1.1.1) not reachable

PC3> ping 10.1.1.2
host (10.1.1.2) not reachable
```

# III. Routing

Dans cette partie, on va donner un peu de sens aux VLANs :

- un pour les serveurs du réseau
  - on simulera ça avec un p'tit serveur web
- un pour les admins du réseau
- un pour les autres random clients du réseau

Cela dit, il faut que tout ce beau monde puisse se ping, au moins joindre le réseau des serveurs, pour accéder au super site-web.

**Bien que bloqué au niveau du switch à cause des VLANs, le trafic pourra passer d'un VLAN à l'autre grâce à un routeur.**

Il assurera son job de routeur traditionnel : router entre deux réseaux. Sauf qu'en plus, il gérera le changement de VLAN à la volée.

## 1. Topologie 3

![Topologie 3](./pics/topo3.png)

## 2. Adressage topologie 3

Les réseaux et leurs VLANs associés :

| Réseau    | Adresse       | VLAN associé |
|-----------|---------------|--------------|
| `clients` | `10.1.1.0/24` | 11           |
| `servers` | `10.2.2.0/24` | 12           |
| `routers` | `10.3.3.0/24` | 13           |

L'adresse des machines au sein de ces réseaux :

| Node               | `clients`       | `admins`        | `servers`       |
|--------------------|-----------------|-----------------|-----------------|
| `pc1.clients.tp4`  | `10.1.1.1/24`   | x               | x               |
| `pc2.clients.tp4`  | `10.1.1.2/24`   | x               | x               |
| `adm1.admins.tp4`  | x               | `10.2.2.1/24`   | x               |
| `web1.servers.tp4` | x               | x               | `10.3.3.1/24`   |
| `r1`               | `10.1.1.254/24` | `10.2.2.254/24` | `10.3.3.254/24` |

## 3. Setup topologie 3

🖥️ VM `web1.servers.tp4`, déroulez la [Checklist VM Linux](#checklist-vm-linux) dessus

🌞 **Adressage**

- définissez les IPs statiques sur toutes les machines **sauf le *routeur***
```
admin> ip 10.2.2.1/24
Checking for duplicate address...
admin : 10.2.2.1 255.255.255.0
```

ip 

🌞 **Configuration des VLANs**

- référez-vous au [mémo Cisco](../../cours/memo/memo_cisco.md#8-vlan)
- déclaration des VLANs sur le switch `sw1`
- ajout des ports du switches dans le bon VLAN (voir [le tableau d'adressage de la topo 2 juste au dessus](#2-adressage-topologie-2))
- il faudra ajouter le port qui pointe vers le *routeur* comme un *trunk* : c'est un port entre deux équipements réseau (un *switch* et un *routeur*)

```
Switch>enable
Switch#conf t
Enter configuration commands, one per line.  End with CNTL/Z.
Switch(config)#vlan 11
Switch(config-vlan)#name clients
Switch(config-vlan)#exit

Switch(config)#vlan 12
Switch(config-vlan)#name admins
Switch(config-vlan)#exit


Switch(config)#vlan 13
Switch(config-vlan)#name servers
Switch(config-vlan)#exit

Switch(config)#interface Gi0/0
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 11
Switch(config-if)#exit
Switch(config)#interface Gi0/1
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 11
Switch(config-if)#exit


Switch(config)#interface Gi0/2
Switch(config-if)#switchport access vlan 12
Switch(config-if)#exit

Switch(config)#interface Gi0/3
Switch(config-if)#switchport access vlan 13
Switch(config-if)#exit

Switch#show vlan br

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Gi1/0, Gi1/1, Gi1/2, Gi1/3
                                                Gi2/0, Gi2/1, Gi2/2, Gi2/3
                                                Gi3/0, Gi3/1, Gi3/2, Gi3/3
10   pc1etpc2                         active
11   clients                          active    Gi0/0, Gi0/1
12   admins                           active    Gi0/2
13   servers                          active    Gi0/3
20   pc3                              active
1002 fddi-default                     act/unsup
1003 token-ring-default               act/unsup
1004 fddinet-default                  act/unsup
1005 trnet-default                    act/unsup


Switch#copy running-config startup-config
Destination filename [startup-config]?
Building configuration...
Compressed configuration from 3695 bytes to 1652 bytes[OK]
*Oct 23 08:27:05.825: %GRUB-5-CONFIG_WRITING: GRUB configuration is being updated on disk. Please wait...
*Oct 23 08:27:06.699: %GRUB-5-CONFIG_WRITTEN: GRUB configuration was written to disk successfully.
```

```

Switch#conf t
Enter configuration commands, one per line.  End with CNTL/Z.
Switch(config)#interface G1/0
Switch(config-if)#switchport trunk encapsulation dot1q
Switch(config-if)#switchport mode trunk
Switch(config-if)#switchport trunk allowed vlan add 11,12,13
Switch(config-if)#exit
Switch(config)#exit
Switch#
*Oct 23 08:50:11.856: %SYS-5-CONFIG_I: Configured from console by console
Switch#show interface trunk

Port        Mode             Encapsulation  Status        Native vlan
Gi1/0       on               802.1q         trunking      1

Port        Vlans allowed on trunk
Gi1/0       1-4094

Port        Vlans allowed and active in management domain
Gi1/0       1,10-13,20

Port        Vlans in spanning tree forwarding state and not pruned
Gi1/0       none

Switch#copy running-config startup-config
Destination filename [startup-config]?
Building configuration...
Compressed configuration from 3756 bytes to 1686 bytes[OK]
Switch#
*Oct 23 08:51:34.127: %GRUB-5-CONFIG_WRITING: GRUB configuration is being updated on disk. Please wait...
*Oct 23 08:51:35.006: %GRUB-5-CONFIG_WRITTEN: GRUB configuration was written to disk successfully.
```
---

➜ **Pour le *routeur***

- référez-vous au [mémo Cisco](../../cours/memo/memo_cisco.md)
- ici, on va avoir besoin d'un truc très courant pour un *routeur* : qu'il porte plusieurs IP sur une unique interface
  - avec Cisco, on crée des "sous-interfaces" sur une interface
  - et on attribue une IP à chacune de ces sous-interfaces
- en plus de ça, il faudra l'informer que, pour chaque interface, elle doit être dans un VLAN spécifique

Pour ce faire, un exemple. On attribue deux IPs `192.168.1.254/24` VLAN 11 et `192.168.2.254` VLAN12 à un *routeur*. L'interface concernée sur le *routeur* est `fastEthernet 0/0` :

```cisco
# conf t

(config)# interface fastEthernet 0/0.10
R1(config-subif)# encapsulation dot1Q 10
R1(config-subif)# ip addr 192.168.1.254 255.255.255.0 
R1(config-subif)# exit

(config)# interface fastEthernet 0/0.20
R1(config-subif)# encapsulation dot1Q 20
R1(config-subif)# ip addr 192.168.2.254 255.255.255.0 
R1(config-subif)# exit
```

🌞 **Config du *routeur***

- attribuez ses IPs au *routeur*
  - 3 sous-interfaces, chacune avec son IP et un VLAN associé

```
R1#conf t
Enter configuration commands, one per line.  End with CNTL/Z.

R1(config)# interface fastEthernet 0/0.11
R1(config-subif)#encapsulation dot1Q 11
R1(config-subif)#ip addr 10.1.1.254 255.255.255.0
R1(config-subif)#exit

R1(config)# interface fastEthernet 0/0.12
R1(config-subif)#encapsulation dot1Q 12
R1(config-subif)#ip addr 10.2.2.254 255.255.255.0
R1(config-subif)#exit

R1(config)# interface fastEthernet 0/0.13
R1(config-subif)#encapsulation dot1Q 13
R1(config-subif)#ip addr 10.3.3.254 255.255.255.0
R1(config-subif)#exit


R1#copy running-config startup-config
Destination filename [startup-config]?
Building configuration...
[OK]
```

🌞 **Vérif**

- tout le monde doit pouvoir ping le routeur sur l'IP qui est dans son réseau
```
R1#conf t
Enter configuration commands, one per line.  End with CNTL/Z.
R1(config)#interface fastEthernet0/0
R1(config-if)#no shut
*Mar  1 00:46:48.683: %LINK-3-UPDOWN: Interface FastEthernet0/0, changed state to up
*Mar  1 00:46:49.683: %LINEPROTO-5-UPDOWN: Line protocol on Interface FastEthernet0/0, changed state to up
R1(config-if)#exit
R1(config)#exit
R1#sh
*Mar  1 00:46:54.883: %SYS-5-CONFIG_I: Configured from console by console
R1#show ip int br
Interface                  IP-Address      OK? Method Status                Protocol
FastEthernet0/0            unassigned      YES unset  up                    up
FastEthernet0/0.11         10.1.1.254      YES manual up                    up
FastEthernet0/0.12         10.2.2.254      YES manual up                    up
FastEthernet0/0.13         10.3.3.254      YES manual up                    up
FastEthernet1/0            unassigned      YES unset  administratively down down
FastEthernet2/0            unassigned      YES unset  administratively down down
FastEthernet3/0            unassigned      YES unset  administratively down down
```

Pour PC1
```
PC1> ping 10.1.1.254

84 bytes from 10.1.1.254 icmp_seq=1 ttl=255 time=10.530 ms
84 bytes from 10.1.1.254 icmp_seq=2 ttl=255 time=4.610 ms
84 bytes from 10.1.1.254 icmp_seq=3 ttl=255 time=14.417 ms
84 bytes from 10.1.1.254 icmp_seq=4 ttl=255 time=15.096 ms
84 bytes from 10.1.1.254 icmp_seq=5 ttl=255 time=17.926 ms
```

Pour PC2
```
PC2> ping 10.1.1.254

84 bytes from 10.1.1.254 icmp_seq=1 ttl=255 time=20.105 ms
84 bytes from 10.1.1.254 icmp_seq=2 ttl=255 time=18.035 ms
84 bytes from 10.1.1.254 icmp_seq=3 ttl=255 time=18.711 ms
84 bytes from 10.1.1.254 icmp_seq=4 ttl=255 time=12.277 ms
84 bytes from 10.1.1.254 icmp_seq=5 ttl=255 time=17.571 ms
```

Pour admin
```
admin> ping 10.2.2.254
84 bytes from 10.2.2.254 icmp_seq=1 ttl=255 time=19.487 ms
84 bytes from 10.2.2.254 icmp_seq=2 ttl=255 time=17.992 ms
84 bytes from 10.2.2.254 icmp_seq=3 ttl=255 time=13.415 ms
84 bytes from 10.2.2.254 icmp_seq=4 ttl=255 time=12.090 ms
84 bytes from 10.2.2.254 icmp_seq=5 ttl=255 time=20.186 ms
```

Pour Web
```
[clement@web1 ~]$ ping 10.3.3.254 -c2
PING 10.3.3.254 (10.3.3.254) 56(84) bytes of data.
64 bytes from 10.3.3.254: icmp_seq=1 ttl=255 time=8.86 ms
64 bytes from 10.3.3.254: icmp_seq=2 ttl=255 time=8.67 ms

--- 10.3.3.254 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 8.672/8.765/8.858/0.093 ms
```

 
- en ajoutant une route vers les réseaux, ils peuvent se ping entre eux
  - ajoutez une route par défaut sur les VPCS
  - ajoutez une route par défaut sur la machine virtuelle
  - testez des `ping` entre les réseaux

```
Switch(config)#ip route 10.1.1.0 255.255.255.0 10.2.2.254
Switch(config)#ip route 0.0.0.0 0.0.0.0 10.2.2.254
Switch(config)#exit

Switch(config)#ip route 10.2.2.0 255.255.255.0 10.1.1.254
Switch(config)#ip route 0.0.0.0 0.0.0.0 10.1.1.254
Switch(config)#exit
```

```
PC1> ip 10.1.1.1/24 10.1.1.254
Checking for duplicate address...
PC1 : 10.1.1.1 255.255.255.0 gateway 10.1.1.254
```
|
|
|
|
|
|
|
||
||
||
||
||
|
|

# IV. NAT

On va ajouter une fonctionnalité au routeur : le NAT.

On va le connecter à internet (simulation du fait d'avoir une IP publique) et il va faire du NAT pour permettre à toutes les machines du réseau d'avoir un accès internet.

![Yellow cable](./pics/yellow-cable.png)

## 1. Topologie 4

![Topologie 3](./pics/topo4.png)

## 2. Adressage topologie 4

Les réseaux et leurs VLANs associés :

| Réseau    | Adresse       | VLAN associé |
|-----------|---------------|--------------|
| `clients` | `10.1.1.0/24` | 11           |
| `servers` | `10.2.2.0/24` | 12           |
| `routers` | `10.3.3.0/24` | 13           |

L'adresse des machines au sein de ces réseaux :

| Node               | `clients`       | `admins`        | `servers`       |
|--------------------|-----------------|-----------------|-----------------|
| `pc1.clients.tp4`  | `10.1.1.1/24`   | x               | x               |
| `pc2.clients.tp4`  | `10.1.1.2/24`   | x               | x               |
| `adm1.admins.tp4`  | x               | `10.2.2.1/24`   | x               |
| `web1.servers.tp4` | x               | x               | `10.3.3.1/24`   |
| `r1`               | `10.1.1.254/24` | `10.2.2.254/24` | `10.3.3.254/24` |

## 3. Setup topologie 4

🌞 **Ajoutez le noeud Cloud à la topo**

- branchez à `eth1` côté Cloud
- côté routeur, il faudra récupérer un IP en DHCP (voir [le mémo Cisco](../../cours/memo/memo_cisco.md))
- vous devriez pouvoir `ping 1.1.1.1`
```
R1#show ip interface brief
Interface                  IP-Address      OK? Method Status                Protocol
FastEthernet0/0            unassigned      YES NVRAM  up                    up
FastEthernet0/0.11         10.1.1.254      YES NVRAM  up                    up
FastEthernet0/0.12         10.2.2.254      YES NVRAM  up                    up
FastEthernet0/0.13         10.3.3.254      YES NVRAM  up                    up
FastEthernet1/0            unassigned      YES NVRAM  administratively down down
FastEthernet2/0            unassigned      YES NVRAM  administratively down down
FastEthernet3/0            unassigned      YES NVRAM  administratively down down
R1#conf t
Enter configuration commands, one per line.  End with CNTL/Z.
R1(config)#interface FastEthernet1/0
R1(config-if)#ip address dhcp
R1(config-if)#no shut
R1(config-if)#exit
R1(config)#exit
R1#show ip interface brief
Interface                  IP-Address      OK? Method Status                Protocol
FastEthernet0/0            unassigned      YES NVRAM  up                    up
FastEthernet0/0.11         10.1.1.254      YES NVRAM  up                    up
FastEthernet0/0.12         10.2.2.254      YES NVRAM  up                    up
FastEthernet0/0.13         10.3.3.254      YES NVRAM  up                    up
FastEthernet1/0            10.0.3.16       YES DHCP   up                    up
FastEthernet2/0            unassigned      YES NVRAM  administratively down down
FastEthernet3/0            unassigned      YES NVRAM  administratively down down

R1# copy running-config startup-config
Destination filename [startup-config]?
Building configuration...
[OK]


R1#ping 1.1.1.1

Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 1.1.1.1, timeout is 2 seconds:
.!!!!
Success rate is 80 percent (4/5), round-trip min/avg/max = 40/58/64 ms
```

🌞 **Configurez le NAT**

- référez-vous [à la section NAT du mémo Cisco](../../cours/memo/memo_cisco.md#7-configuration-dun-nat-simple)

```

R1#show ip int br
Interface                  IP-Address      OK? Method Status                Protocol
FastEthernet0/0            unassigned      YES NVRAM  up                    up
FastEthernet0/0.11         10.1.1.254      YES NVRAM  up                    up
FastEthernet0/0.12         10.2.2.254      YES NVRAM  up                    up
FastEthernet0/0.13         10.3.3.254      YES NVRAM  up                    up
FastEthernet1/0            10.0.3.16       YES DHCP   up                    up
FastEthernet2/0            unassigned      YES NVRAM  administratively down down
FastEthernet3/0            unassigned      YES NVRAM  administratively down down
R1#conf t
Enter configuration commands, one per line.  End with CNTL/Z.
R1(config)#interface fastEthernet1/0
R1(config-if)#ip nat outside

*Mar  1 00:07:31.339: %LINEPROTO-5-UPDOWN: Line protocol on Interface NVI0, changed state to up
R1(config-if)#
*Mar  1 00:07:38.067: %SYS-3-CPUHOG: Task is running for (2032)msecs, more than (2000)msecs (0/0),process = Exec.
-Traceback= 0x612CADA8 0x612C9CF4 0x612CA974 0x61292050 0x61292310 0x61292434 0x61292434 0x61293304 0x612C6154 0x612D234C 0x612BC744 0x612BD3A8 0x612BE2F8 0x60F0A454 0x6040914C 0x60425410
*Mar  1 00:07:38.503: %SYS-3-CPUYLD: Task ran for (2468)msecs, more than (2000)msecs (0/0),process = Exec
R1(config-if)#exit


R1#conf t
Enter configuration commands, one per line.  End with CNTL/Z.
R1(config)#interface fastEthernet0/0
R1(config-if)#ip nat inside
R1(config-if)#exit
R1(config)#access-list 1 permit any
R1(config)# ip nat inside source list 1 interface FastEthernet1/0 overload

R1# copy running-config startup-config
Destination filename [startup-config]?
Building configuration...
[OK]

```


🌞 **Test**

- ajoutez une route par défaut (si c'est pas déjà fait)
  - sur les VPCS
  - sur la machine Linux
- configurez l'utilisation d'un DNS
  - sur les VPCS
  - sur la machine Linux
- vérifiez un `ping` vers un nom de domaine  

Pour PC1
```
PC1> ip 10.1.1.1/24 10.1.1.254
Checking for duplicate address...
PC1 : 10.1.1.1 255.255.255.0 gateway 10.1.1.254

PC1> show ip
NAME        : PC1[1]
IP/MASK     : 10.1.1.1/24
GATEWAY     : 10.1.1.254
DNS         :
MAC         : 00:50:79:66:68:00
LPORT       : 20041
RHOST:PORT  : 127.0.0.1:20042
MTU         : 1500


PC1> ip dns 1.1.1.1

PC1> save
Saving startup configuration to startup.vpc
.  done


PC1> ping google.com
google.com resolved to 142.250.179.110

84 bytes from 142.250.179.110 icmp_seq=1 ttl=117 time=31.307 ms
84 bytes from 142.250.179.110 icmp_seq=2 ttl=117 time=32.279 ms
84 bytes from 142.250.179.110 icmp_seq=3 ttl=117 time=37.200 ms
84 bytes from 142.250.179.110 icmp_seq=4 ttl=117 time=31.992 ms
84 bytes from 142.250.179.110 icmp_seq=5 ttl=117 time=37.832 ms
```

Pour PC2
```
PC2> ip 10.1.1.2/24 10.1.1.254
Checking for duplicate address...
PC2 : 10.1.1.2 255.255.255.0 gateway 10.1.1.254

PC2> show ip

NAME        : PC2[1]
IP/MASK     : 10.1.1.2/24
GATEWAY     : 10.1.1.254
DNS         :
MAC         : 00:50:79:66:68:01
LPORT       : 20043
RHOST:PORT  : 127.0.0.1:20044
MTU         : 1500


PC2> ip dns 1.1.1.1

PC2> save
Saving startup configuration to startup.vpc
.  done


PC2> ping google.com
google.com resolved to 142.250.179.110

84 bytes from 142.250.179.110 icmp_seq=1 ttl=117 time=30.841 ms
84 bytes from 142.250.179.110 icmp_seq=2 ttl=117 time=36.118 ms
84 bytes from 142.250.179.110 icmp_seq=3 ttl=117 time=29.119 ms
84 bytes from 142.250.179.110 icmp_seq=4 ttl=117 time=29.208 ms
84 bytes from 142.250.179.110 icmp_seq=5 ttl=117 time=30.655 ms
```

Pour Admin
```
admin> ip 10.2.2.1/24 10.2.2.254
Checking for duplicate address...
admin : 10.2.2.1 255.255.255.0 gateway 10.2.2.254

admin> show ip

NAME        : admin[1]
IP/MASK     : 10.2.2.1/24
GATEWAY     : 10.2.2.254
DNS         :
MAC         : 00:50:79:66:68:02
LPORT       : 20045
RHOST:PORT  : 127.0.0.1:20046
MTU         : 1500

admin> ip dns 1.1.1.1

admin> ping google.com
google.com resolved to 142.250.179.110

84 bytes from 142.250.179.110 icmp_seq=1 ttl=117 time=44.300 ms
84 bytes from 142.250.179.110 icmp_seq=2 ttl=117 time=39.283 ms
84 bytes from 142.250.179.110 icmp_seq=3 ttl=117 time=41.201 ms
84 bytes from 142.250.179.110 icmp_seq=4 ttl=117 time=39.739 ms
84 bytes from 142.250.179.110 icmp_seq=5 ttl=117 time=40.973 ms

admin> save
Saving startup configuration to startup.vpc
.  done
```

Pour le server Web
```
[clement@web1 ~]$ sudo cat /etc/sysconfig/network-scripts/ifcfg-enp0s3
TYPE=Ethernet
BOOTPROTO=static
NAME=enp0s3
IPADDR=10.3.3.1
NETMASK=255.255.255.0
DEVICE=enp0s3
ONBOOT=yes
DNS1=1.1.1.1

[clement@web1 ~]$ sudo nmcli con reload
[clement@web1 ~]$ sudo nmcli con up enp0s3

[clement@web1 ~]$ sudo ip route add default via 10.3.3.254 dev enp0s3

[clement@web1 ~]$ ping 10.3.3.254 -c2
PING 10.3.3.254 (10.3.3.254) 56(84) bytes of data.
64 bytes from 10.3.3.254: icmp_seq=1 ttl=255 time=11.2 ms
64 bytes from 10.3.3.254: icmp_seq=2 ttl=255 time=16.7 ms

--- 10.3.3.254 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 11.179/13.950/16.722/2.774 ms
[clement@web1 ~]$ ping 8.8.8.8
connect: Network is unreachable
[clement@web1 ~]$ ping google.com
ping: google.com: Name or service not known
```



# V. Add a building

On a acheté un nouveau bâtiment, faut tirer et configurer un nouveau switch jusque là-bas.

On va en profiter pour setup un serveur DHCP pour les clients qui s'y trouvent.

## 1. Topologie 5

![Topo 5](./pics/topo5.png)

## 2. Adressage topologie 5

Les réseaux et leurs VLANs associés :

| Réseau    | Adresse       | VLAN associé |
|-----------|---------------|--------------|
| `clients` | `10.1.1.0/24` | 11           |
| `servers` | `10.2.2.0/24` | 12           |
| `routers` | `10.3.3.0/24` | 13           |

L'adresse des machines au sein de ces réseaux :

| Node                | `clients`       | `admins`        | `servers`       |
|---------------------|-----------------|-----------------|-----------------|
| `pc1.clients.tp4`   | `10.1.1.1/24`   | x               | x               |
| `pc2.clients.tp4`   | `10.1.1.2/24`   | x               | x               |
| `pc3.clients.tp4`   | DHCP            | x               | x               |
| `pc4.clients.tp4`   | DHCP            | x               | x               |
| `pc5.clients.tp4`   | DHCP            | x               | x               |
| `dhcp1.clients.tp4` | `10.1.1.253/24` | x               | x               |
| `adm1.admins.tp4`   | x               | `10.2.2.1/24`   | x               |
| `web1.servers.tp4`  | x               | x               | `10.3.3.1/24`   |
| `r1`                | `10.1.1.254/24` | `10.2.2.254/24` | `10.3.3.254/24` |

## 3. Setup topologie 5

Vous pouvez partir de la topologie 4. 

🌞  **Vous devez me rendre le `show running-config` de tous les équipements**

- de tous les équipements réseau
  - le routeur
  - les 3 switches

> N'oubliez pas les VLANs sur tous les switches.

🖥️ **VM `dhcp1.client1.tp4`**, déroulez la [Checklist VM Linux](#checklist-vm-linux) dessus

🌞  **Mettre en place un serveur DHCP dans le nouveau bâtiment**

- il doit distribuer des IPs aux clients dans le réseau `clients` qui sont branchés au même switch que lui
- sans aucune action manuelle, les clients doivent...
  - avoir une IP dans le réseau `clients`
  - avoir un accès au réseau `servers`
  - avoir un accès WAN
  - avoir de la résolution DNS

> Réutiliser les serveurs DHCP qu'on a monté dans les autres TPs.

🌞  **Vérification**

- un client récupère une IP en DHCP
- il peut ping le serveur Web
- il peut ping `8.8.8.8`
- il peut ping `google.com`

> Faites ça sur n'importe quel VPCS que vous venez d'ajouter : `pc3` ou `pc4` ou `pc5`.

![i know cisco](./pics/i_know.jpeg)
