# Tp1 - Réseaux B

# I. Exploration locale en solo
### 1. Affichage d'informations sur la pile TCP/IP locale**

*🌞 Affichez les infos des cartes réseau de votre PC*

```
PS C:\Users\AUGIER de MOUSSAC> Get-NetAdapter -Name * -Physical

Name                      InterfaceDescription                    ifIndex Status       MacAddress          LinkSpeed
----                      --------------------                    ------- ------       ----------          ---------
Ethernet                  Realtek PCIe GbE Family Controller            9 Disconnected 98-28-A6-2C-17-F0       0 bps
Wi-Fi                     Intel(R) Wireless-AC 9560 160MHz              6 Up           18-56-80-70-9C-48    400 Mbps

```
**Pour la carte Wifi :**
- Nom : Wi-fi
- Adresse MAc : 18-56-80-70-9C-48
- Adresse ip : 10.33.2.120



```
PS C:\Users\AUGIER de MOUSSAC> ipconfig
 Carte Ethernet Ethernet :

   Statut du média. . . . . . . . . . . . : Média déconnecté
   Suffixe DNS propre à la connexion. . . : home

Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . : auvence.co
   Adresse IPv6 de liaison locale. . . . .: fe80::b9:861:2258:1576%6
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.2.120
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Passerelle par défaut. . . . . . . . . : 10.33.3.253

```
**Pour la carte Ethernet :**
- Nom : Ethernet
- Adresse MAc : 98-28-A6-2C-17-F0
- Adresse ip : nous n'avons aucune ip car nous ne sommes pas connecté à un réseau via un cable ethernet

*🌞 Affichez votre gateway*

```
PS C:\Users\AUGIER de MOUSSAC> Get-NetIPConfiguration | Foreach IPv4DefaultGateway

ifIndex DestinationPrefix                              NextHop                                  RouteMetric ifMetric
------- -----------------                              -------                                  ----------- --------
6       0.0.0.0/0                                      10.33.3.253                                        0 35

```

L'adresse ip de la gateway de la carte wifi est 10.33.3.253

*🌞 Trouvez comment afficher les informations sur une carte IP (change selon l'OS)*

![](https://i.imgur.com/wofnCNs.png)



*🌞 à quoi sert la gateway dans le réseau d'YNOV ?*

Elle permet de relier le reseau d'ynov au réseau d'internet. Cela permet donc aux hôtes présent sur le réseaux d'ynov d'accéder à internet (donc de sortir du réseaux)

### 2.Modifications des informations
- #### A. Modification d'adresse IP (part 1)

*🌞Utilisez l'interface graphique de votre OS pour changer d'adresse IP :*

- Avant le changement 
```
PS C:\Users\AUGIER de MOUSSAC> ipconfig
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . : auvence.co
   Adresse IPv6 de liaison locale. . . . .: fe80::b9:861:2258:1576%6
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.2.120
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Passerelle par défaut. . . . . . . . . : 10.33.3.253

```

- Après le changement via l'interface GUI
```
PS C:\Users\AUGIER de MOUSSAC> ipconfig
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::b9:861:2258:1576%6
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.2.122
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
```

*🌞 Il est possible que vous perdiez l'accès internet. Que ce soit le cas ou non, expliquez pourquoi c'est possible de perdre son accès internet en faisant cette opération.*

J'ai en effet perdu l'accès au réseaux. Cela pourrait venir du fait que l'adresse ip est déjà été attribué. Il y aurait donc un conflit d'adresse ip.

- #### B. Table ARP

*🌞 Exploration de la table ARP*
```
PS C:\Users\AUGIER de MOUSSAC> arp -a

Interface : 10.33.2.120 --- 0x6
  Adresse Internet      Adresse physique      Type
  10.33.2.62            b0-7d-64-b1-98-d3     dynamique
  10.33.3.33            c8-58-c0-63-5a-92     dynamique
  10.33.3.81            3c-58-c2-9d-98-38     dynamique
  10.33.3.253           00-12-00-40-4c-bf     dynamique
  10.33.3.254           00-0e-c4-cd-74-f5     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

Interface : 10.10.20.1 --- 0x7
  Adresse Internet      Adresse physique      Type
  10.10.20.255          ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff  
     statique

Interface : 192.168.120.1 --- 0xc
  Adresse Internet      Adresse physique      Type
  192.168.120.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

Interface : 169.254.208.222 --- 0xe
  Adresse Internet      Adresse physique      Type
  169.254.255.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

Interface : 169.254.41.159 --- 0xf
  Adresse Internet      Adresse physique      Type
  169.254.255.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

Interface : 192.168.56.1 --- 0x14
  Adresse Internet      Adresse physique      Type
  192.168.56.255        ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

Interface : 10.10.10.1 --- 0x18
  Adresse Internet      Adresse physique      Type
  10.10.10.255          ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
```
L'adresse Mac de la passerelle de mon réseau est c8-58-c0-63-5a-92. Sachant que mon adresse ip (10.33.2.120) communique avec la passerrelle je suis sur que dans la table arp de mon adresse ip se trouvera l'ip de la passerrelle (10.33.3.33) donc son adresse MAc sera également présente.

*🌞 Et si on remplissait un peu la table ?*


- #### C. nmap
*🌞Utilisez nmap pour scanner le réseau de votre carte WiFi et trouver une adresse IP libre*

Voici le nmap effectué :
```

PS C:\Program Files (x86)\Nmap> .\nmap.exe -sP 10.33.2.120/22
Starting Nmap 7.92 ( https://nmap.org ) at 2021-09-16 09:17 Paris, Madrid (heure dÆÚtÚ)
Nmap scan report for 10.33.0.6
Host is up (0.084s latency).
MAC Address: 84:5C:F3:80:32:07 (Intel Corporate)
Nmap scan report for 10.33.0.7
Host is up (0.13s latency).
MAC Address: 9C:BC:F0:B6:1B:ED (Xiaomi Communications)
Nmap scan report for 10.33.0.19
Host is up (0.055s latency).
MAC Address: D4:A3:3D:C8:CA:67 (Apple)
Nmap scan report for 10.33.0.21
Host is up (0.016s latency).
MAC Address: B0:FC:36:CE:9C:89 (CyberTAN Technology)
Nmap scan report for 10.33.0.27
Host is up (0.013s latency).
MAC Address: A8:64:F1:8B:1D:4D (Intel Corporate)
Nmap scan report for 10.33.0.31
Host is up (0.46s latency).
MAC Address: FA:C5:6D:60:4B:4C (Unknown)
Nmap scan report for 10.33.0.43
Host is up (0.11s latency).
MAC Address: 2C:8D:B1:94:38:BF (Intel Corporate)
Nmap scan report for 10.33.0.57
Host is up (0.16s latency).
MAC Address: F0:18:98:8C:6F:CD (Apple)
Nmap scan report for 10.33.0.71
Host is up (0.041s latency).
MAC Address: F0:03:8C:35:FE:47 (AzureWave Technology)
Nmap scan report for 10.33.0.100
Host is up (0.035s latency).
MAC Address: E8:6F:38:6A:B6:EF (Chongqing Fugui Electronics)
Nmap scan report for 10.33.0.111
Host is up (0.97s latency).
MAC Address: D2:41:F0:DC:6A:ED (Unknown)
Nmap scan report for 10.33.0.117
Host is up (0.33s latency).
MAC Address: 8A:D8:25:FF:2E:A6 (Unknown)
Nmap scan report for 10.33.0.135
Host is up (0.22s latency).
MAC Address: F8:5E:A0:06:40:D2 (Intel Corporate)
Nmap scan report for 10.33.0.140
Host is up (0.011s latency).
MAC Address: 40:EC:99:8B:11:C2 (Intel Corporate)
```

Voici la table d'arp:
```
PS C:\Users\AUGIER de MOUSSAC> arp -a

Interface : 10.33.2.120 --- 0x6
  Adresse Internet      Adresse physique      Type
  10.33.0.1             3c-58-c2-9d-98-38     dynamique
  10.33.0.7             9c-bc-f0-b6-1b-ed     dynamique
  10.33.0.43            2c-8d-b1-94-38-bf     dynamique
  10.33.0.60            e2-ee-36-a5-0b-8a     dynamique
  10.33.0.96            ca-4f-f4-af-8f-0c     dynamique
  10.33.0.140           40-ec-99-8b-11-c2     dynamique
  10.33.0.180           26-91-29-98-e2-9d     dynamique
  10.33.0.228           7c-5c-f8-2d-40-42     dynamique
  10.33.0.242           74-4c-a1-51-1e-61     dynamique
  10.33.0.250           28-cd-c4-dd-db-73     dynamique
  10.33.0.251           48-e7-da-41-bf-e1     dynamique
  10.33.1.1             70-66-55-6c-1b-d1     dynamique
  10.33.1.48            34-42-62-23-f7-3c     dynamique
  10.33.1.70            30-57-14-94-de-fb     dynamique
  10.33.1.166           1a-41-0b-54-a5-a0     dynamique
  10.33.1.194           20-16-b9-84-86-1d     dynamique
  10.33.1.228           34-7d-f6-b2-82-cf     dynamique
  10.33.1.238           50-76-af-88-6c-0b     dynamique
  10.33.1.243           34-7d-f6-5a-20-da     dynamique
  10.33.1.248           e8-84-a5-24-94-c9     dynamique
  10.33.2.4             f0-9e-4a-64-4b-2d     dynamique
  10.33.2.48            80-91-33-9c-bf-0d     dynamique
  10.33.2.51            a8-64-f1-37-d1-15     dynamique
  10.33.2.88            68-3e-26-7c-c3-1a     dynamique
  10.33.2.173           34-2e-b7-47-f9-28     dynamique
  10.33.2.205           a4-83-e7-6a-3f-64     dynamique
  10.33.2.216           08-d2-3e-35-00-a2     dynamique
  10.33.2.241           9c-6b-72-9b-aa-8b     dynamique
  10.33.3.52            d8-f2-ca-0e-a8-44     dynamique
  10.33.3.59            02-47-cd-3d-d4-e9     dynamique
  10.33.3.80            3c-58-c2-9d-98-38     dynamique
  10.33.3.89            f0-9e-4a-52-94-f0     dynamique
  10.33.3.117           d0-c5-d3-8c-6c-f9     dynamique
  10.33.3.139           34-cf-f6-37-2c-fb     dynamique
  10.33.3.187           96-fd-87-13-4b-ee     dynamique
  10.33.3.218           8c-85-90-65-6a-52     dynamique
  10.33.3.239           38-f9-d3-ae-c3-a3     dynamique
  10.33.3.248           34-7d-f6-5a-89-99     dynamique
  10.33.3.253           00-12-00-40-4c-bf     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
```

L'adresse ip 10.33.0.119 semble actuellement libre d'après le scan de ping fait avec nmap. Cette adresse n'étant pas présent sur notre table arp cela confirme notre hypothèse.

#### D. Modification d'adresse IP (part 2)
L'adresse utilisé afin de changer d'adresse ip est : 10.33.0.119

Voici la commande nmap et son résultat 

```
PS C:\Program Files (x86)\Nmap> .\nmap.exe -sP 10.33.0.119/22
Starting Nmap 7.92 ( https://nmap.org ) at 2021-09-16 09:24 Paris, Madrid (heure dÆÚtÚ)
Nmap scan report for 10.33.0.6
Host is up (0.024s latency).
MAC Address: 84:5C:F3:80:32:07 (Intel Corporate)
Nmap scan report for 10.33.0.7
Host is up (0.26s latency).
MAC Address: 9C:BC:F0:B6:1B:ED (Xiaomi Communications)
Nmap scan report for 10.33.0.19
Host is up (0.85s latency).
MAC Address: D4:A3:3D:C8:CA:67 (Apple)
Nmap scan report for 10.33.0.31
Host is up (0.73s latency).
MAC Address: FA:C5:6D:60:4B:4C (Unknown)
Nmap scan report for 10.33.0.43
Host is up (0.0040s latency).
MAC Address: 2C:8D:B1:94:38:BF (Intel Corporate)
Nmap scan report for 10.33.0.48
Host is up (0.37s latency).
MAC Address: 54:4E:90:3C:33:3F (Apple)
Nmap scan report for 10.33.0.57
Host is up (0.099s latency).
MAC Address: F0:18:98:8C:6F:CD (Apple)
Nmap scan report for 10.33.0.58
Host is up (0.15s latency).
MAC Address: 80:30:49:B9:F4:3D (Liteon Technology)
Nmap scan report for 10.33.0.60
Host is up (0.37s latency).
MAC Address: E2:EE:36:A5:0B:8A (Unknown)
Nmap scan report for 10.33.0.78
Host is up (0.14s latency).
MAC Address: EC:63:D7:C7:51:87 (Intel Corporate)
Nmap scan report for 10.33.0.85
Host is up (0.25s latency).
MAC Address: 9E:08:36:E0:C5:22 (Unknown)
Nmap scan report for 10.33.0.96
Host is up (0.021s latency).
MAC Address: CA:4F:F4:AF:8F:0C (Unknown)
Nmap scan report for 10.33.0.100
Host is up (0.012s latency).
MAC Address: E8:6F:38:6A:B6:EF (Chongqing Fugui Electronics)
Nmap scan report for 10.33.0.111
Host is up (0.053s latency).
MAC Address: D2:41:F0:DC:6A:ED (Unknown)
Nmap scan report for 10.33.0.117
Host is up (0.32s latency).
MAC Address: 8A:D8:25:FF:2E:A6 (Unknown)
Nmap scan report for 10.33.0.130
Host is up (0.0070s latency).
MAC Address: E4:5E:37:07:22:22 (Intel Corporate)
Nmap scan report for 10.33.0.133
Host is up (0.017s latency).
MAC Address: 1C:BF:C0:17:32:09 (Chongqing Fugui Electronics)
Nmap scan report for 10.33.0.135
```

```
PS C:\Program Files (x86)\Nmap> ipconfig /all
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Intel(R) Wireless-AC 9560 160MHz
   Adresse physique . . . . . . . . . . . : 18-56-80-70-9C-48
   DHCP activé. . . . . . . . . . . . . . : Non
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::b9:861:2258:1576%6(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.0.119(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
   IAID DHCPv6 . . . . . . . . . . . : 51926656
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-23-77-8C-CF-98-28-A6-2C-17-F0
   Serveurs DNS. . .  . . . . . . . . . . : 1.1.1.1
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé
```
Nous pouvons voir que la ligne DHCP activé affiche bien Non. Cela prouve que nous avons bien adressé notre adresse ip manuellement  

```
PS C:\Program Files (x86)\Nmap> ping 10.33.3.253

Envoi d’une requête 'Ping'  10.33.3.253 avec 32 octets de données :
Réponse de 10.33.3.253 : octets=32 temps=3 ms TTL=255
Réponse de 10.33.3.253 : octets=32 temps=2 ms TTL=255
Réponse de 10.33.3.253 : octets=32 temps=12 ms TTL=255
Réponse de 10.33.3.253 : octets=32 temps=2 ms TTL=255

Statistiques Ping pour 10.33.3.253:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 2ms, Maximum = 12ms, Moyenne = 4ms
```
Nous arrivons à ping notre passerrelle. Cela confirme que celle-ci est bien définie

```
PS C:\Program Files (x86)\Nmap> ping 8.8.8.8

Envoi d’une requête 'Ping'  8.8.8.8 avec 32 octets de données :
Réponse de 8.8.8.8 : octets=32 temps=18 ms TTL=115
Réponse de 8.8.8.8 : octets=32 temps=18 ms TTL=115
Réponse de 8.8.8.8 : octets=32 temps=18 ms TTL=115
Réponse de 8.8.8.8 : octets=32 temps=18 ms TTL=115

Statistiques Ping pour 8.8.8.8:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 18ms, Maximum = 18ms, Moyenne = 18ms
PS C:\Program Files (x86)\Nmap> ping google.com

Envoi d’une requête 'ping' sur google.com [216.58.214.174] avec 32 octets de données :
Réponse de 216.58.214.174 : octets=32 temps=19 ms TTL=114
Réponse de 216.58.214.174 : octets=32 temps=18 ms TTL=114
Réponse de 216.58.214.174 : octets=32 temps=18 ms TTL=114
Réponse de 216.58.214.174 : octets=32 temps=19 ms TTL=114

Statistiques Ping pour 216.58.214.174:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 18ms, Maximum = 19ms, Moyenne = 18ms
```
Nous avons bien accès à internet car nous ponvons ping les serveurs de google.


# II. Exploration locale en duo
### 3. Modification d'adresse IP
Mon binome pour la réalisation de cette partie est Arthur Muraro

adresse ip utilisé pour le pc 1 : **192.168.10.1/30**
adresse ip utilisé pour le pc 2 : **192.168.10.2/30**

```
PS C:\Users\AUGIER de MOUSSAC> ping 192.168.137.2

Envoi d’une requête 'Ping'  192.168.137.2 avec 32 octets de données :
Réponse de 192.168.137.2 : octets=32 temps<1ms TTL=64
Réponse de 192.168.137.2 : octets=32 temps<1ms TTL=64
Réponse de 192.168.137.2 : octets=32 temps<1ms TTL=64
Réponse de 192.168.137.2 : octets=32 temps<1ms TTL=64

Statistiques Ping pour 192.168.137.2:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 0ms, Moyenne = 0ms
```
Nous arrivons à communiquer car le ping fonctionne


L'adresse de la machine de mon voisin apparait bien dans la tabl arp

```
PS C:\Users\AUGIER de MOUSSAC> arp -a
Interface : 192.168.137.1 --- 0x9
  Adresse Internet      Adresse physique      Type
  192.168.137.2         44-a9-2c-50-03-40     dynamique
```

#### 4. Utilisation d'un des deux comme gateway
Etant la machine qui sert de routeur j'ai accès à internet

```
PS C:\Users\AUGIER de MOUSSAC> ping 8.8.8.8

Envoi d’une requête 'Ping'  8.8.8.8 avec 32 octets de données :
Réponse de 8.8.8.8 : octets=32 temps=22 ms TTL=115
Réponse de 8.8.8.8 : octets=32 temps=19 ms TTL=115
Réponse de 8.8.8.8 : octets=32 temps=17 ms TTL=115
Réponse de 8.8.8.8 : octets=32 temps=19 ms TTL=115

Statistiques Ping pour 8.8.8.8:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 17ms, Maximum = 22ms, Moyenne = 19ms
 ``` 

 Faisons le tracert
 ```
 PS C:\Users\AUGIER de MOUSSAC> tracert 8.8.8.8

Détermination de l’itinéraire vers dns.google [8.8.8.8]
avec un maximum de 30 sauts :

  1     4 ms     2 ms     2 ms  10.33.3.253
  2     2 ms     2 ms     2 ms  10.33.10.254
  3     7 ms     7 ms     7 ms  reverse.completel.net [92.103.174.137]
  4    12 ms    11 ms    10 ms  92.103.120.182
  5    19 ms    17 ms    22 ms  172.19.130.117
  6    18 ms    16 ms    19 ms  46.218.128.74
  7    20 ms    17 ms    18 ms  38.147.6.194.rev.sfr.net [194.6.147.38]
  8    19 ms    17 ms    17 ms  72.14.194.30
  9    20 ms    18 ms    17 ms  172.253.69.49
 10    20 ms    19 ms    29 ms  142.250.224.93
 11    19 ms    17 ms    20 ms  dns.google [8.8.8.8]

Itinéraire déterminé.
```


#### 5. Petit chat privé
Arthur Muraro est le serveur.
Je suis le client
```
PS C:\Users\AUGIER de MOUSSAC\Downloads\netcat-win32-1.11\netcat-1.11> .\nc.exe 192.168.137.2 8888
salut
Léo c'est le plus beau
tu trouves ? jprefere devilledon
Oh le batart
allez jme casse t'as des gouts de chiottes
```

*🌞 pour aller un peu plus loin*
Cette fois-ci je suis le serveur et arthur le client
```
PS C:\Users\AUGIER de MOUSSAC\Downloads\netcat-win32-1.11\netcat-1.11> .\nc.exe -l -p 8888 192.168.137.2
hello
ciao !
```

>**Partie 5 de Arthur Muraro** 
>
>### ­ƒî× sur le PC serveur avec l'IP 192.168.137.2
>
>```bash
>Ô×£  ~ netcat -l -p 8888
>salut
>L├®o c'est le plus beau
>tu trouves ? jprefere devilledon
>Oh le batart
>allez jme casse t'as des gouts de chiottes
>```
>
>### ­ƒî× sur le PC client avec l'IP 192.168.137.1
>
>commande de cl├®ment : (c'est lui le client) : `nc.exe 192.168.137.2 8888`
>
>### ­ƒî× pour aller un peu plus loin
>
>commande de cl├®ment : (c'est lui le serveur) : `nc.exe -l -p 192.168.137.2 8888`
>
>```bash
>Ô×£  ~ netcat 192.168.137.1 8888
>hello
>ciao !
>```


#### 6. Firewall
On active notre firewall
```
PS C:\WINDOWS\system32> netsh advfirewall set allprofiles state on
Ok.
```

*🌞 Autoriser les ping*
```
PS C:\WINDOWS\system32> netsh advfirewall firewall add rule name="ICMP Allow incoming V4 echo request" protocol="icmpv4:8,any" dir=in action=allow
Ok.
```

*🌞 Autoriser nc sur un port spécifique*
Autoristion faite sur le port 1050

```
PS C:\WINDOWS\system32> New-NetFirewallRule -DisplayName "ALLOW TCP PORT 1050" -Direction inbound -Profile Any -Action Allow -LocalPort 1050 -Protocol TCP


Name                  : {fbe67276-46d8-4209-99e7-2a8fa85e6948}
DisplayName           : ALLOW TCP PORT 1050
Description           :
DisplayGroup          :
Group                 :
Enabled               : True
Profile               : Any
Platform              : {}
Direction             : Inbound
Action                : Allow
EdgeTraversalPolicy   : Block
LooseSourceMapping    : False
LocalOnlyMapping      : False
Owner                 :
PrimaryStatus         : OK
Status                : La règle a été analysée à partir de la banque. (65536)
EnforcementStatus     : NotApplicable
PolicyStoreSource     : PersistentStore
PolicyStoreSourceType : Local
```

Utilisons à présent ce port pour communiquer (je suis le serveur et arthur le client)
```
PS C:\Users\AUGIER de MOUSSAC\Downloads\netcat-win32-1.11\netcat-1.11> .\nc.exe -l -p 1050 192.168.137.2
un ruc propre
ciao !
```

# III. Manipulations d'autres outils/protocoles côté client

Cette partie du Tp à été réalisée sur mon réseau personnel 
### 1. DHCP
*🌞Exploration du DHCP, depuis votre PC*

```
PS C:\Users\AUGIER de MOUSSAC> ipconfig /all
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Intel(R) Wireless-AC 9560 160MHz
   Adresse physique . . . . . . . . . . . : 18-56-80-70-9C-48
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   Adresse IPv6. . . . . . . . . . . . . .: 2a01:e0a:6:7590:b9:861:2258:1576(préféré)
   Adresse IPv6 temporaire . . . . . . . .: 2a01:e0a:6:7590:20b5:642d:afac:710a(préféré)
   Adresse IPv6 de liaison locale. . . . .: fe80::b9:861:2258:1576%6(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.0.26(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
   Bail obtenu. . . . . . . . . . . . . . : dimanche 19 septembre 2021 14:13:57
   Bail expirant. . . . . . . . . . . . . : lundi 20 septembre 2021 02:13:59
   Passerelle par défaut. . . . . . . . . : fe80::f6ca:e5ff:fe4e:7304%6
                                       192.168.0.254
   Serveur DHCP . . . . . . . . . . . . . : 192.168.0.254
   IAID DHCPv6 . . . . . . . . . . . : 51926656
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-23-77-8C-CF-98-28-A6-2C-17-F0
   Serveurs DNS. . .  . . . . . . . . . . : 1.1.1.1
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé
```
- Adresse ip du serveur dhcp : ```Serveur DHCP . . . . . . . . . . . . . : 192.168.0.254```
- Date expiration : ```Bail expirant. . . . . . . . . . . . . : lundi 20 septembre 2021 02:13:59```


### 2. DNS
*🌞 trouver l'adresse IP du serveur DNS que connaît votre ordinateur*
```
PS C:\Users\AUGIER de MOUSSAC> ipconfig /all
    Carte réseau sans fil Wi-Fi :
   Serveurs DNS. . .  . . . . . . . . . . : 1.1.1.1
```

*🌞 utiliser, en ligne de commande l'outil nslookup (Windows, MacOS) ou dig (GNU/Linux, MacOS) pour faire des requêtes DNS à la main*

```
PS C:\Users\AUGIER de MOUSSAC> nslookup google.com
Serveur :   one.one.one.one
Address:  1.1.1.1

Réponse ne faisant pas autorité :
Nom :    google.com
Addresses:  2a00:1450:4007:818::200e
          172.217.18.206

PS C:\Users\AUGIER de MOUSSAC> nslookup ynov.com
Serveur :   one.one.one.one
Address:  1.1.1.1

Réponse ne faisant pas autorité :
Nom :    ynov.com
Address:  92.243.16.143
```

Pour google.com : 
  - adresse ip : 172.217.18.206

Pour ynov.com : 
 - adressse ip : 92.243.16.143

Interprétation du résultat des lignes de commandes :
Nslookup nous renvoie l'adresse du dns qui à été utilisé, le nom de domain que l'on cherche ainsi que l'adresse ip et adresse MAC associé à ce nom de domain pour le dns qui nous est attribué. 

Adresse IP du serveur à qui vous venez d'effectuer ces requêtes : 1.1.1.1
```
PS C:\Users\AUGIER de MOUSSAC> nslookup ynov.com
Serveur :   one.one.one.one
Address:  1.1.1.1
```


Passons au *reverse lookup*
```
PS C:\Users\AUGIER de MOUSSAC> Resolve-DnsName 78.74.21.21

Name                           Type   TTL   Section    NameHost
----                           ----   ---   -------    --------
21.21.74.78.in-addr.arpa       PTR    3600  Answer     host-78-74-21-21.homerun.telia.com


PS C:\Users\AUGIER de MOUSSAC> Resolve-DnsName 92.146.54.88

Name                           Type   TTL   Section    NameHost
----                           ----   ---   -------    --------
88.54.146.92.in-addr.arpa      PTR    3600  Answer     apoitiers-654-1-167-88.w92-146.abo.wanadoo.fr
```

Interprétation du résultat des lignes de commandes :
La commande ```
Resolve-DnsName``` nous renvoie l'adresse ip demandée, son nom de domaine, le type de la demande effectué (ici ptr pour reverse lookup), le temps d'enregistrement du dns dans le cache d'un serveur (un TTL de 3600 équivaut à 1 heure) et le type de la section (ici la section Answer).


# IV. Wireshark
*🌞 utilisez le pour observer les trames qui circulent entre vos deux carte Ethernet. Mettez en évidence :*

- un ping entre vous et la passerelle
![](https://i.imgur.com/tczn8yp.png)



- une requête DNS. Identifiez dans la capture le serveur DNS à qui vous posez la question.
![](https://i.imgur.com/PVk7DsH.png)

