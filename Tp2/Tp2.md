## 0. Prérequis

Pour ce TP, on va se servir de VMs Rocky Linux. 1Go RAM c'est large large. Vous pouvez redescendre la mémoire vidéo aussi.  

Vous aurez besoin de deux réseaux host-only dans VirtualBox :

- un premier réseau `10.2.1.0/24`
- le second `10.2.2.0/24`
- **vous devrez désactiver le DHCP de votre hyperviseur (VirtualBox) et définir les IPs de vos VMs de façon statique**

Quelques paquets seront souvent nécessaires dans les TPs, il peut être bon de les installer dans la VM que vous clonez :

- de quoi avoir les commandes :
  - `dig`
  - `tcpdump`
  - `nmap`
  - `nc`
  - `vim` peut être une bonne idée

Vous devrez **systématiquement** utiliser SSH pour contrôler vos VMs.

Vos firewalls doivent **toujours** être actifs (et donc correctement configurés).

Aussi, **autre rappel**, vos machines doivent **toujours être nommées** ([définition du hostname](../../cours/memo/rocky_network.md#changer-son-nom-de-domaine)).

Dernière chose : **pour toutes les parties avec Wireshark**, vous devrez systématiquement fournir le fichier `.pcap` dans le rendu de TP. Pour le compte-rendu, vous pouvez screen Wireshark si besoin. Un petit emoji 📁 sera présent à chaque fois qu'une capture devra être dans le rendu.

## I. ARP
### 1. Echange ARP

🌞**Générer des requêtes ARP**

- effectuer un `ping` d'une machine à l'autre
```
[clement@node1 ~]$ ping 10.2.1.12 -c4
PING 10.2.1.12 (10.2.1.12) 56(84) bytes of data.
64 bytes from 10.2.1.12: icmp_seq=1 ttl=64 time=0.739 ms
64 bytes from 10.2.1.12: icmp_seq=2 ttl=64 time=1.11 ms
64 bytes from 10.2.1.12: icmp_seq=3 ttl=64 time=0.922 ms
64 bytes from 10.2.1.12: icmp_seq=4 ttl=64 time=0.709 ms

--- 10.2.1.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3084ms
rtt min/avg/max/mdev = 0.709/0.870/1.110/0.160 ms
```

- observer les tables ARP des deux machines
table ARP de node1.net1.tp2
```
[clement@node1 ~]$ arp -a
? (10.2.1.1) at 0a:00:27:00:00:60 [ether] on enp0s8
_gateway (10.0.2.2) at 52:54:00:12:35:02 [ether] on enp0s3
? (10.2.1.12) at 08:00:27:82:43:29 [ether] on enp0s8
```
table ARP de node2.net1.tp2
```
[clement@node2 ~]$ arp -a
? (10.2.1.11) at 08:00:27:97:a6:4d [ether] on enp0s8
? (10.2.1.1) at 0a:00:27:00:00:60 [ether] on enp0s8
? (10.2.1.1) at 0a:00:27:00:00:60 [ether] on enp0s8
```
- repérer l'adresse MAC de `node1` dans la table ARP de `node2` et vice-versa
  - L'adresse Mac de node2.net1.tp2 est : 08:00:27:82:43:29
  - L'adresse Mac de node1.net1.tp2 est : 08:00:27:97:a6:4d


- prouvez que l'info est correcte (que l'adresse MAC que vous voyez dans la table est bien celle de la machine correspondante)
  - une commande pour voir la table ARP de `node1`
  - et une commande pour voir la MAC de `node2`
```
[clement@node1 ~]$ arp -a
? (10.2.1.1) at 0a:00:27:00:00:60 [ether] on enp0s8
_gateway (10.0.2.2) at 52:54:00:12:35:02 [ether] on enp0s3
? (10.2.1.12) at 08:00:27:82:43:29 [ether] on enp0s8
```
  ```
[clement@node2 ~]$ ip a
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:82:43:29 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.12/24 brd 10.2.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe82:4329/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```
L'adresse mac étant 08:00:27:82:43:29. On peut conclure que l'information est bien correcte.

### 2. Analyse de trames

🌞**Analyse de trames**

- utilisez la commande `tcpdump` pour réaliser une capture de trame

Sur Node1 :
```
[clement@node1 ~]$ sudo tcpdump -i enp0s8 -w trameNode1.pcap
[sudo] password for clement:
dropped privs to tcpdump
tcpdump: listening on enp0s8, link-type EN10MB (Ethernet), capture size 262144 bytes
```

Sur Node2 : 
```
clement@node2 ~]$ sudo tcpdump -i enp0s8 -w trameNode2.pcap
[sudo] password for clement:
dropped privs to tcpdump
tcpdump: listening on enp0s8, link-type EN10MB (Ethernet), capture size 262144 bytes
```

- videz vos tables ARP, sur les deux machines, puis effectuez un `ping`

Sur Node1 :
```
[clement@node1 ~]$ sudo ip -s -s neigh flush all
10.0.2.2 dev enp0s3 lladdr 52:54:00:12:35:02 used 1317/1317/1278 probes 4 STALE
10.2.1.12 dev enp0s8 lladdr 08:00:27:82:43:29 used 30090/30088/30065 probes 1 STALE
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:0b ref 1 used 0/0/2 probes 1 DELAY

*** Round 1, deleting 3 entries ***
*** Flush is complete after 1 round ***
```
Sur Node2 :
```
[clement@node2 ~]$ sudo ip -s -s neigh flush all
[sudo] password for clement:
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:0b ref 1 used 26/0/21 probes 1 REACHABLE
10.0.2.2 dev enp0s3 lladdr 52:54:00:12:35:02 used 1476/1476/1454 probes 4 STALE
10.2.1.11 dev enp0s8 lladdr 08:00:27:97:a6:4d used 30250/30250/30234 probes 1 STALE

*** Round 1, deleting 3 entries ***
*** Flush is complete after 1 round ***
```

A présent effectuons un ping de 10.2.1.12 (node2) depuis 10.2.1.11 (node1).
```[clement@node1 ~]$ ping 10.2.1.12
PING 10.2.1.12 (10.2.1.12) 56(84) bytes of data.
64 bytes from 10.2.1.12: icmp_seq=1 ttl=64 time=0.764 ms
64 bytes from 10.2.1.12: icmp_seq=2 ttl=64 time=2.03 ms
64 bytes from 10.2.1.12: icmp_seq=3 ttl=64 time=1.51 ms
64 bytes from 10.2.1.12: icmp_seq=4 ttl=64 time=1.13 ms
^C
--- 10.2.1.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3017ms
rtt min/avg/max/mdev = 0.764/1.355/2.028/0.470 ms
```

A présent effectuons un ping de 10.2.1.11 (node1) depuis 10.2.1.12
```
[clement@node2 ~]$ ping 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=64 time=1.12 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=64 time=0.656 ms
64 bytes from 10.2.1.11: icmp_seq=3 ttl=64 time=1.36 ms
64 bytes from 10.2.1.11: icmp_seq=4 ttl=64 time=0.819 ms
64 bytes from 10.2.1.11: icmp_seq=5 ttl=64 time=0.560 ms
^C
--- 10.2.1.11 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4115ms
rtt min/avg/max/mdev = 0.560/0.901/1.357/0.298 ms
```

- stoppez la capture, et exportez-la sur votre hôte pour visualiser avec Wireshark
- mettez en évidence les trames ARP
- **écrivez, dans l'ordre, les échanges ARP qui ont eu lieu, je veux TOUTES les trames**

Node 1 : 
| ordre | type trame  | source                   | destination                |
|-------|-------------|--------------------------|----------------------------|
| 1     | Requête ARP | `node1` `08:00:27:97:a6:4d` | Broadcast `FF:FF:FF:FF:FF` |
| 2     | Réponse ARP | `inconnu` `0A:00:27:00:00:0b` | `node1` `node1` `08:00:27:97:a6:4d`   |
| 3     | Réponse ARP | `node2` `08:00:27:82:43:29` | Broadcast `FF:FF:FF:FF:FF`   |
| 4     | Réponse ARP | `node1` `08:00:27:97:a6:4d` | Broadcast `FF:FF:FF:FF:FF`   |
| 5     | Réponse ARP | `node2` `08:00:27:82:43:29` | `node1` `08:00:27:97:a6:4d` |
| 6     | Réponse ARP | `node2` `08:00:27:82:43:29` | `node1` `08:00:27:97:a6:4d` |
| 7     | Réponse ARP | `node1` `08:00:27:97:a6:4d` | `node2` `08:00:27:82:43:29`   |


Node 2 :
| ordre | type trame  | source                   | destination                |
|-------|-------------|--------------------------|----------------------------|
| 1     | Requête ARP | `node2` `08:00:27:82:43:29` | Broadcast `FF:FF:FF:FF:FF` |
| 2     | Réponse ARP | `inconnu` `0A:00:27:00:00:0b` | `node2` `node2` `08:00:27:82:43:29`  |
| 3     | Réponse ARP | `node2` `08:00:27:82:43:29` | Broadcast `FF:FF:FF:FF:FF`   |
| 4     | Réponse ARP | `node1` `08:00:27:97:a6:4d` | `node2` `node2` `08:00:27:82:43:29`   |
| 5     | Réponse ARP | `node1` `08:00:27:97:a6:4d` | `node2` `node2` `08:00:27:82:43:29`  |
| 6     | Réponse ARP | `node2` `node2` `08:00:27:82:43:29` | `node1` `08:00:27:97:a6:4d`   |



> Si vous ne savez pas comment récupérer votre fichier `.pcap` sur votre hôte afin de l'ouvrir dans Wireshark, demandez-moi.

## II. Routage

Vous aurez besoin de 3 VMs pour cette partie.

| Machine           | NAT ? | `10.2.1.0/24` | `10.2.2.0/24` |
|-------------------|-------|---------------|---------------|
| `router.net2.tp2` | oui   | `10.2.1.11`   | `10.2.2.11`   |
| `node1.net1.tp2`  | no    | `10.2.1.12`   | no            |
| `marcel.net2.tp2` | no    | no            | `10.2.2.12`   |

> Je l'ai appelé `marcel` histoire de voir un peu plus clair dans les noms 🌻

```schema
   node1               router              marcel
  ┌─────┐             ┌─────┐             ┌─────┐
  │     │    ┌───┐    │     │    ┌───┐    │     │
  │     ├────┤ho1├────┤     ├────┤ho2├────┤     │
  └─────┘    └───┘    └─────┘    └───┘    └─────┘
```

### 1. Mise en place du routage

🌞**Activer le routage sur le noeud `router.2.tp2`**

> Cette étape est nécessaire car Rocky Linux c'est pas un OS dédié au routage par défaut. Ce n'est bien évidemment une opération qui n'est pas nécessaire sur un équipement routeur dédié comme du matériel Cisco.

```
[clement@router ~]$ sudo firewall-cmd --add-masquerade
success
[clement@router ~]$ sudo firewall-cmd --add-masquerade --permanent
success
```

🌞**Ajouter les routes statiques nécessaires pour que `node1.net1.tp2` et `marcel.net2.tp2` puissent se `ping`**

- il faut ajouter une seule route des deux côtés
- une fois les routes en place, vérifiez avec un `ping` que les deux machines peuvent se joindre

Sur node1:
```
[clement@node1 network-scripts]$  sudo ip route add 10.2.2.0/24 via 10.2.1.11 dev enp0s8
[clement@node1 network-scripts]$ ip r
10.2.1.0/24 dev enp0s8 proto kernel scope link src 10.2.1.12 metric 100
10.2.2.0/24 via 10.2.1.11 dev enp0s8
```

Sur marcel:
```
[clement@marcel network-scripts]$ sudo ip route add 10.2.1.0/24 via 10.2.2.11 dev enp0s8
[sudo] password for clement:
[clement@marcel network-scripts]$ ip r
10.2.1.0/24 via 10.2.2.11 dev enp0s8
10.2.2.0/24 dev enp0s8 proto kernel scope link src 10.2.2.12 metric 100
```

Ping de marcel depuis node1 : 
```
[clement@node1 network-scripts]$ ping 10.2.2.12 -c4
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=1.11 ms
64 bytes from 10.2.2.12: icmp_seq=2 ttl=63 time=1.31 ms
64 bytes from 10.2.2.12: icmp_seq=3 ttl=63 time=0.912 ms
64 bytes from 10.2.2.12: icmp_seq=4 ttl=63 time=0.960 ms

--- 10.2.2.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3040ms
rtt min/avg/max/mdev = 0.912/1.072/1.313/0.161 ms
```

Ping de node1 depuis marcel : 
```
[clement@marcel network-scripts]$ ping 10.2.1.11 -c4
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=64 time=0.413 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=64 time=0.262 ms
64 bytes from 10.2.1.11: icmp_seq=3 ttl=64 time=0.531 ms
64 bytes from 10.2.1.11: icmp_seq=4 ttl=64 time=0.640 ms

--- 10.2.1.11 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3104ms
rtt min/avg/max/mdev = 0.262/0.461/0.640/0.142 ms
```

### 2. Analyse de trames

🌞**Analyse des échanges ARP**

- videz les tables ARP des trois noeuds
node 1 : 
```
[clement@node1 network-scripts]$ sudo ip neigh flush all
[sudo] password for clement:
```

marcel : 
```
[clement@marcel network-scripts]$ sudo ip neigh flush all
[sudo] password for clement:
```

router: 
```
[clement@router ~]$ sudo ip neigh flush all
[sudo] password for clement:
```

- effectuez un `ping` de `node1.net1.tp2` vers `marcel.net2.tp2`
```
[clement@node1 network-scripts]$ ping 10.2.2.12 -c4
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=1.42 ms
64 bytes from 10.2.2.12: icmp_seq=2 ttl=63 time=0.711 ms
64 bytes from 10.2.2.12: icmp_seq=3 ttl=63 time=0.965 ms
64 bytes from 10.2.2.12: icmp_seq=4 ttl=63 time=1.07 ms

--- 10.2.2.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3065ms
rtt min/avg/max/mdev = 0.711/1.042/1.420/0.254 ms
```

- regardez les tables ARP des trois noeuds
node1 : 
```
[clement@node1 network-scripts]$ ip neigh show
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:0b REACHABLE
10.2.1.11 dev enp0s8 lladdr 08:00:27:0b:91:2f REACHABLE
```

marcel : 
```
[clement@marcel network-scripts]$ ip neigh show
10.2.2.11 dev enp0s8 lladdr 08:00:27:32:3c:09 REACHABLE
10.2.2.1 dev enp0s8 lladdr 0a:00:27:00:00:06 DELAY
```

router : 
```
[clement@router ~]$ ip neigh show
10.2.1.12 dev enp0s8 lladdr 08:00:27:97:a6:4d REACHABLE
10.2.2.12 dev enp0s9 lladdr 08:00:27:82:43:29 REACHABLE
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:0b DELAY
```

- essayez de déduire un peu les échanges ARP qui ont eu lieu

Node1 effectue un arp request vers notre router. Celui-ci lui répond avec un arp-reply. Après lque le ping soit effectué entre node1 et le routeur. Le routeur envoie une requete Arp à marcel et marcel répond avec un apr reply . Un pin est par la suite envoyé de marcel vers le router. Pour finir le pong est envoye de marcel au routeur puis du routeur à node1.


- répétez l'opération précédente (vider les tables, puis `ping`), en lançant `tcpdump` sur les 3 noeuds, afin de capturer les échanges depuis les 3 points de vue

Node1 : 
```
[clement@node1 network-scripts]$ sudo tcpdump -i enp0s8 -w trameNode1P2.pcap
dropped privs to tcpdump
tcpdump: listening on enp0s8, link-type EN10MB (Ethernet), capture size 262144 bytes
^C256 packets captured
257 packets received by filter
0 packets dropped by kernel
```

Marcel
- **écrivez, dans l'ordre, les échanges ARP qui ont eu lieu, puis le ping et le pong, je veux TOUTES les trames**

Par exemple (copiez-collez ce tableau ce sera le plus simple) :

| ordre | type trame              | IP source | MAC source                   | IP destination | MAC destination            |
|-------|-------------------------|-----------|------------------------------|----------------|----------------------------|
| 1     | Requête ARP request     | 10.2.1.12 | `node1` `08:00:27:97:a6:4d`  | 10.2.1.11      | `Router` `08:00:27:0b:91:2f`|
| 2     | Réponse ARP reply       | 10.2.1.11 | `router` `08:00:27:0b:91:2f` | 10.2.1.12      | `node1` `08:00:27:97:a6:4d`|
| 3     | Réponse ARP request     | 10.2.2.11 | `router` `08:00:27:32:3c:09` | 10.2.2.12      | `marcel` `08:00:27:82:43:29`|
| 4     | Réponse ARP reply       | 10.2.2.12 | `marcel` `08:00:27:82:43:29` |  10.2.2.11     | `router` `08:00:27:32:3c:09`|
| 6     | Ping                    | 10.2.1.12 | `node1` `08:00:27:97:a6:4d`  | 10.2.2.12      | `marcel` `08:00:27:82:43:29`  |
| 7     | Pong                    | 10.2.2.12 | `marcel` `08:00:27:82:43:29`  | 10.2.1.12     |`node1` `08:00:27:97:a6:4d`  |

📁 Capture `tp2_routage_node1.pcap` `tp2_routage_marcel.pcap` `tp2_routage_router.pcap`

### 3. Accès internet

🌞**Donnez un accès internet à vos machines**

- le routeur a déjà un accès internet
```
[clement@router ~]$ ping 8.8.8.8 -c2
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=20.1 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=18.4 ms

--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 18.417/19.246/20.075/0.829 ms
[clement@router ~]$ ping google.com -c2
PING google.com (216.58.214.78) 56(84) bytes of data.
64 bytes from par10s39-in-f14.1e100.net (216.58.214.78): icmp_seq=1 ttl=114 time=18.3 ms
64 bytes from par10s39-in-f14.1e100.net (216.58.214.78): icmp_seq=2 ttl=114 time=18.3 ms

--- google.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 18.276/18.288/18.301/0.135 ms
```

- ajoutez une route par défaut à `node1.net1.tp2` et `marcel.net2.tp2` 

Pour node 1
```
[clement@node1 ~]$ sudo ip route add default via 10.2.1.11 dev enp0s8
[clement@node1 ~]$ ip r
default via 10.2.1.11 dev enp0s8
10.2.1.0/24 dev enp0s8 proto kernel scope link src 10.2.1.12 metric 100
10.2.2.0/24 via 10.2.1.11 dev enp0s8
```

Pour Marcel : 
```
[clement@marcel ~]$ sudo ip route add default via 10.2.2.11 dev enp0s8
[sudo] password for clement:
[clement@marcel ~]$ ip r
default via 10.2.2.11 dev enp0s8
10.2.1.0/24 via 10.2.2.11 dev enp0s8
10.2.2.0/24 dev enp0s8 proto kernel scope link src 10.2.2.12 metric 100
```

  - vérifiez que vous avez accès internet avec un `ping`
  - le `ping` doit être vers une IP, PAS un nom de domaine
Pour node 1
```
[clement@node1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=18.8 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=19.3 ms
```

Pour Marcel : 
```
[clement@marcel ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=117 time=12.6 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=117 time=12.2 ms
```

- donnez leur aussi l'adresse d'un serveur DNS qu'ils peuvent utiliser
  - vérifiez que vous avez une résolution de noms qui fonctionne avec `dig`
  - puis avec un `ping` vers un nom de domaine

Pour node1
```
[clement@node1 ~]$ cat /etc/resolv.conf
# Generated by NetworkManager
search auvence.co net1.tp2
nameserver 1.1.1.1

[clement@node1 ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 20326
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             189     IN      A       216.58.198.206

;; Query time: 16 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Thu Sep 23 13:19:34 CEST 2021
;; MSG SIZE  rcvd: 55

[clement@node1 ~]$ ping google.com
PING google.com (216.58.209.238) 56(84) bytes of data.
64 bytes from par10s29-in-f238.1e100.net (216.58.209.238): icmp_seq=1 ttl=117 time=12.7 ms
64 bytes from par10s29-in-f238.1e100.net (216.58.209.238): icmp_seq=2 ttl=117 time=12.1 ms
```

Pour Marcel:
```
[clement@marcel ~]$ cat /etc/resolv.conf
# Generated by NetworkManager
search auvence.co net2.tp2

[clement@marcel ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 17105
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             152     IN      A       172.217.19.238

;; Query time: 15 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Thu Sep 23 13:25:05 CEST 2021
;; MSG SIZE  rcvd: 55


[clement@marcel ~]$ ping google.com
PING google.com (142.250.75.238) 56(84) bytes of data.
64 bytes from par10s41-in-f14.1e100.net (142.250.75.238): icmp_seq=1 ttl=117 time=11.9 ms
64 bytes from par10s41-in-f14.1e100.net (142.250.75.238): icmp_seq=2 ttl=117 time=12.7 ms
```

🌞Analyse de trames

- effectuez un `ping 8.8.8.8` depuis `node1.net1.tp2`
- capturez le ping depuis `node1.net1.tp2` avec `tcpdump`
- analysez un ping aller et le retour qui correspond et mettez dans un tableau :

| ordre | type trame | IP source           | MAC source                   | IP destination         | MAC destination |     |
|-------|------------|---------------------|------------------------------|------------------------|-----------------|-----|
| 1     | ping       | `node1` `10.2.1.12` | `node1` `08:00:27:97:a6:4d` | `8.8.8.8`              | `routeur` `08:00:27:0b:91:2f`|     |
| 2     | pong       | `8.8.8.8`          | `routeur` `08:00:27:0b:91:2f`| `node1` `10.2.1.12`    |  `node1` `08:00:27:97:a6:4d`|  |

📁 Capture `tp2_routage_internet.pcap`

## III. DHCP

On reprend la config précédente, et on ajoutera à la fin de cette partie une 4ème machine pour effectuer des tests.

| Machine           | NAT ? | `10.2.1.0/24`              | `10.2.2.0/24` |
|-------------------|-------|----------------------------|---------------|
| `router.net2.tp2` | oui   | `10.2.1.11`                | `10.2.2.11`   |
| `node1.net1.tp2`  | no    | `10.2.1.12`                | no            |
| `node2.net1.tp2`  | no    | oui mais pas d'IP statique | no            |
| `marcel.net2.tp2` | no    | no                         | `10.2.2.12`   |

```schema
   node1               router              marcel
  ┌─────┐             ┌─────┐             ┌─────┐
  │     │    ┌───┐    │     │    ┌───┐    │     │
  │     ├────┤ho1├────┤     ├────┤ho2├────┤     │
  └─────┘    └─┬─┘    └─────┘    └───┘    └─────┘
   node2       │
  ┌─────┐      │
  │     │      │
  │     ├──────┘
  └─────┘
```

### 1. Mise en place du serveur DHCP

🌞**Sur la machine `node1.net1.tp2`, vous installerez et configurerez un serveur DHCP** (go Google "rocky linux dhcp server").

- installation du serveur sur `node1.net1.tp2`
```
[clement@node1 ~]$ sudo dnf -y install dhcp-server
```

- créer une machine `node2.net1.tp2`
- faites lui récupérer une IP en DHCP à l'aide de votre serveur

Sur node1:
```
[clement@node1 ~]$ sudo  systemctl enable --now dhcpd

[clement@node1 ~]$ sudo firewall-cmd --add-service=dhcp
success

[clement@node1 ~]$ sudo firewall-cmd --runtime-to-permanent
success
```

Sur node2
```
[clement@node2 ~]$ sudo nano ifcfg-enp0s8

[clement@node2 ~]$ sudo nmcli con reload

[clement@node2 ~]$ sudo nmcli con up enp0s8

[clement@node2 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:3d:9b:09 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.200/24 brd 10.2.1.255 scope global dynamic noprefixroute enp0s8
       valid_lft 491sec preferred_lft 491sec
    inet6 fe80::a00:27ff:fe3d:9b09/64 scope link
       valid_lft forever preferred_lft forever
```


> Il est possible d'utilise la commande `dhclient` pour forcer à la main, depuis la ligne de commande, la demande d'une IP en DHCP, ou renouveler complètement l'échange DHCP (voir `dhclient -h` puis call me et/ou Google si besoin d'aide).

🌞**Améliorer la configuration du DHCP**

- ajoutez de la configuration à votre DHCP pour qu'il donne aux clients, en plus de leur IP :
  - une route par défaut
  - un serveur DNS à utiliser
- récupérez de nouveau une IP en DHCP sur `node2.net1.tp2` pour tester :
  - `node2.net1.tp2` doit avoir une IP
```
[clement@node2 ~]$ sudo dhclient -r enp0s8
[sudo] password for clement:

[clement@node2 ~]$ sudo dhclient enp0s8

[clement@node2 ~]$ sudo nmcli con reload

[clement@node2 ~]$ sudo nmcli con up enp0s8
```
    - vérifier avec une commande qu'il a récupéré son IP

```
[clement@node2 ~]$ ip a
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:3d:9b:09 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.201/24 brd 10.2.1.255 scope global dynamic noprefixroute enp0s8
       valid_lft 507sec preferred_lft 507sec
    inet6 fe80::a00:27ff:fe3d:9b09/64 scope link
       valid_lft forever preferred_lft forever
```
Node2 a bien récuperer une nouvelle adresse (avant 10.2.1.200 maintenant 10.2.1.201 )

  - vérifier qu'il peut `ping` sa passerelle
```
[clement@node2 ~]$ ip r
default via 10.2.1.11 dev enp0s8 proto dhcp metric 100
10.2.1.0/24 dev enp0s8 proto kernel scope link src 10.2.1.201 metric 100
[clement@node2 ~]$ ping 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=64 time=0.922 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=64 time=0.612 ms
64 bytes from 10.2.1.11: icmp_seq=3 ttl=64 time=0.471 ms
```

  - il doit avoir une route par défaut
  - vérifier la présence de la route avec une commande
```
[clement@node2 ~]$ ip r
default via 10.2.1.11 dev enp0s8 proto dhcp metric 100
10.2.1.0/24 dev enp0s8 proto kernel scope link src 10.2.1.201 metric 100
```
    - vérifier que la route fonctionne avec un `ping` vers une IP
```
[clement@node2 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=117 time=22.6 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=117 time=17.1 ms
```

  - il doit connaître l'adresse d'un serveur DNS pour avoir de la résolution de noms
    - vérifier avec la commande `dig` que ça fonctionne
    - vérifier un `ping` vers un nom de domaine
```

[clement@node2 ~]$ dig ynov.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 1737
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;ynov.com.                      IN      A

;; ANSWER SECTION:
ynov.com.               1473    IN      A       92.243.16.143

;; Query time: 14 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Sun Sep 26 12:22:14 CEST 2021
;; MSG SIZE  rcvd: 53

[clement@node2 ~]$ ping google.com
PING google.com (142.250.75.238) 56(84) bytes of data.
64 bytes from par10s41-in-f14.1e100.net (142.250.75.238): icmp_seq=1 ttl=117 time=13.6 ms
64 bytes from par10s41-in-f14.1e100.net (142.250.75.238): icmp_seq=2 ttl=117 time=12.8 ms
^C
--- google.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2005ms
rtt min/avg/max/mdev = 12.843/13.145/13.583/0.343 ms
```

🌞**Analyse de trames**

- videz les tables ARP des machines concernées
Sur node1
```
[clement@node1 ~]$ sudo ip -s -s neigh flush all
[sudo] password for clement:
10.2.1.11 dev enp0s8 lladdr 08:00:27:0b:91:2f used 3460/3455/3423 probes 1 STALE
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:0b ref 1 used 2/0/1 probes 1 REACHABLE
10.2.1.201 dev enp0s8 lladdr 08:00:27:3d:9b:09 used 158/152/127 probes 1 STALE

*** Round 1, deleting 3 entries ***
*** Flush is complete after 1 round ***
```

Sur Node2
```
[clement@node2 ~]$ sudo ip -s -s neigh flush all
10.2.1.12 dev enp0s8 lladdr 08:00:27:97:a6:4d used 137/132/109 probes 1 STALE
10.2.1.11 dev enp0s8 lladdr 08:00:27:0b:91:2f used 53/48/25 probes 1 STALE
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:0b ref 1 used 16/0/15 probes 4 REACHABLE

*** Round 1, deleting 3 entries ***
*** Flush is complete after 1 round ***
```

- lancer une capture à l'aide de `tcpdump` afin de capturer un échange DHCP
  - choisissez vous-mêmes l'interface où lancer la capture
- répéter une opération de renouvellement de bail DHCP, et demander une nouvelle IP afin de générer un échange DHCP
- exportez le fichier `.pcap`

```
[clement@node1 ~]$ sudo tcpdump -i enp0s8 -w trameNode2Dhcp.pcap
dropped privs to tcpdump
tcpdump: listening on enp0s8, link-type EN10MB (Ethernet), capture size 262144 bytes
^C57 packets captured
58 packets received by filter
0 packets dropped by kernel
```
- mettez en évidence l'échange DHCP *DORA* (Disover, Offer, Request, Acknowledge)
- **écrivez, dans l'ordre, les échanges ARP + DHCP qui ont lieu, je veux TOUTES les trames** utiles pour l'échange 

| ordre | type trame              | IP source | MAC source                   | IP destination  | MAC destination             |
|-------|-------------------------|-----------|------------------------------|-----------------|-----------------------------|
| 1     | DHCP Discover           | 10.2.1.201| `Node2` `08:00:27:3d:9b:09`  | 255.255.255.255 | `Broadcast` `FF:FF:FF:FF:FF`|
| 2     | DHCP Offer              | 10.2.1.12 | `node1` `08:00:27:97:a6:4d`  | 10.2.1.201      | `Node2` `08:00:27:3d:9b:09` |
| 3     | DHCP Request            | 10.2.1.201| `Node2` `08:00:27:3d:9b:09`  | 255.255.255.255 | `Broadcast` `FF:FF:FF:FF:FF`|
| 4     | DHCP Ark                | 10.2.1.12 | `node1` `08:00:27:97:a6:4d`  | 10.2.1.201      | `Node2` `08:00:27:3d:9b:09` |
| 5     | Réponse ARP request     | 10.2.1.201| `Node2` `08:00:27:3d:9b:09`  | 255.255.255.255 | `Broadcast` `FF:FF:FF:FF:FF`|
| 6     | Réponse ARP reply       | 10.2.1.12 | `node1` `08:00:27:97:a6:4d`  | 10.2.1.201      | `Node2` `08:00:27:3d:9b:09` |
| 7     | Réponse ARP reply       | 10.2.1.201| `Node2` `08:00:27:3d:9b:09`  |  10.2.1.12      | `node1` `08:00:27:97:a6:4d` |


📁 Capture réseau `tp2_dhcp.pcap`

## Conclusion

Dans ce TP, on a exploré :

- le fonctionnement du protocole ARP, nécessaire à la communication au sein d'un LAN
- le fonctionnement du protocole IP, nécessaire pour le routage
  - p'tit big up au DHCP au passage
- le fait que des protocoles communs comme DNS ou HTTP utilise toute cette couche