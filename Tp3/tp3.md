# TP3 : Progressons vers le réseau d'infrastructure

Ce qu'on entend par "réseau" d'infrastructure c'est d'**adopter un point de vue admin** plutôt qu'un point de vue client.

Nous allons nous **positionner** de plus en plus comme les **personnes qui entretiennent le réseau** (admin), et non simplement comme les **personnes qui consomment le réseau** (client).

De quoi est composé, sommairement un réseau digne de ce nom :

- **une architecture réseau**
  - c'est l'organisation des switches, routeurs, firewall, etc, ainsi que le câblage entre eux
  - redondance des équipements, des logiciels, des solutions
  - on en verra que peu pour le moment
- **des services réseau d'infra**
  - des services très récurrents, nécessaire au fonctionnement normal d'un parc info
  - on parle ici, entre autres, de : DNS, DHCP, etc.
- **de services réseau orientés sur le maintien en condition opérationnelle**
  - accès aux machines via SSH
  - monitoring
  - sauvegarde
- **d'autres services, utiles directement aux clients du réseau**
  - serveur web
  - serveur de partage de fichiers

**BAH.** On est partis :D

![Here we gooo](./pic/sysadmin-hotline.gif "Here we gooo")

# Sommaire

- [TP3 : Progressons vers le réseau d'infrastructure](#tp3--progressons-vers-le-réseau-dinfrastructure)
- [Sommaire](#sommaire)
- [0. Prérequis](#0-prérequis)
- [I. (mini)Architecture réseau](#i-miniarchitecture-réseau)
- [II. Services d'infra](#ii-services-dinfra)
  - [1. Serveur DHCP](#1-serveur-dhcp)
  - [2. Serveur DNS](#2-serveur-dns)
    - [A. Our own DNS server](#a-our-own-dns-server)
    - [B. SETUP copain](#b-setup-copain)
  - [3. Get deeper](#3-get-deeper)
    - [A. DNS forwarder](#a-dns-forwarder)
    - [B. On revient sur la conf du DHCP](#b-on-revient-sur-la-conf-du-dhcp)
- [Entracte](#entracte)
- [III. Services métier](#iii-services-métier)
  - [1. Serveur Web](#1-serveur-web)
  - [2. Partage de fichiers](#2-partage-de-fichiers)
    - [A. L'introduction wola](#a-lintroduction-wola)
    - [B. Le setup wola](#b-le-setup-wola)
- [IV. Un peu de théorie : TCP et UDP](#iv-un-peu-de-théorie--tcp-et-udp)
- [V. El final](#v-el-final)

# 0. Prérequis

➜ On reste sur full Rocky Linux côté machines virtuelles pour le tp : les clients, les routeurs, les serveurs. All Rocky meow. **N'hésitez pas à descendre à 1024Mo de RAM, voire moins. On a pas besoin de grand chose, et il va y avoir plusieurs machines dans ce TP.**

➜ [Référez-vous au mémo dédié pour les paquets à pré-installer dans le patron, ainsi que la conf à y effectuer.](../../cours/memo/install_vm.md)

➜ **Aucune carte NAT, à part sur la machine `router`** qui donnera un accès internet à tout le monde.

➜ A chaque machine déployée, vous **DEVREZ** vérifier la 📝**checklist**📝 :

- [x] IP locale, statique ou dynamique
- [x] hostname défini
- [x] firewall actif, qui ne laisse passer que le strict nécessaire
- [x] SSH fonctionnel
- [x] accès Internet (une route par défaut donc :) )
  - le routeur a une carte NAT
  - les autres machines passent par le routeur pour avoir internet (ajout de route par défaut)
- [x] accès à tous les LANs
  - route statiques ou dynamiques, suivant le type de noeud (voir un peu plus bas)
- [x] résolution de nom
  - vers internet
  - en local, grâce à votre propre serveur DNS (une fois qu'il sera monté, au début, ignorez cette étape)
- [x] modification des fichiers de zone DNS
  - attendez d'avoir passer l'étape du DNS pour faire ça à chaque fois
  - au début, ignorez cette étape

**Les éléments de la 📝checklist📝 sont STRICTEMENT OBLIGATOIRES à réaliser mais ne doivent PAS figurer dans le rendu.**

➜ On distinguera trois types de noeuds pendant le TP :

- *routeurs* :
  - IP statiques
  - routes statiques si besoin
- *serveurs* :
  - IP statiques
  - routes statiques si besoin
- *clients* :
  - IP dynamiques (récupérées en DHCP)
  - routes dynamiques (récupérées en DHCP)

# I. (mini)Architecture réseau

**Le but ici va être de mettre en place 3 réseaux, dont vous choisirez vous-mêmes les adresses.** Les contraintes :

- chaque réseau doit commencer par `10.3.`
  - `10` -> adresse privée
  - `3` -> c'est notre troisième TP :)
- chaque réseau doit être **le plus petit possible** : choisissez judicieusement vos masques
- aucun réseau ne doit se superposer (pas de doublons d'adresses IP possible)
- il y aura
  - deux réseaux pour des serveurs
  - un réseau pour des clients
- en terme de taille de réseau, vous compterez
  - 35 clients max pour le réseau `client1`
  - 63 serveurs max pour le réseau `server1`
  - 10 serveurs max pour le réseau `server2`
- les réseaux doivent, autant que possible, se suivre
- [référez-vous au cours dédié au subnetting](../../cours/ip/README.md#2-deuxième-exemple-subnetting) si vous ne savez pas comment vous y prendre

> *Comme d'hab, pour le moment, chaque réseau = un host-only dans VirtualBox. Vous attribuerez comme IP à votre carte host-only (sur l'hôte, votre PC) **la première IP disponible du réseau.***

---

**Il y aura un routeur dans le TP**, qui acheminera les paquets d'un réseau à l'autre. Ce routeur sera donc la **passerelle** des 3 réseaux.  
On peut aussi dire qu'il sera la passerelle des machines, dans chacun des réseaux.  

> *Cette machine aura donc une carte réseau dans TOUS les réseaux.*

**Pour ce routeur, vous respecterez la convention qui consiste à lui attribuer la dernière IP disponible du réseau dans lequel il se trouve.**

> *Par exemple, dans le réseau `192.168.10.0/24`, par convention, on définira sur le routeur l'adresse IP `192.168.10.254`.*

---

🌞 **Vous me rendrez un 🗃️ tableau des réseaux 🗃️ qui rend compte des adresses choisies, sous la forme** :

| Nom du réseau | Adresse du réseau | Masque            | Nombre de clients possibles | Adresse passerelle | [Adresse broadcast](../../cours/lexique/README.md#adresse-de-diffusion-ou-broadcast-address) |
|---------------|-------------------|-------------------|-----------------------------|--------------------|----------------------------------------------------------------------------------------------|
| `client1`     | `10.3.1.0`        | `255.255.255.192` |            62               | `10.3.1.62`        | `10.3.1.63`                                                                                   |
| `server1`     | `10.3.2.0`        | `255.255.255.128` |            126              | `10.3.2.126`       | `10.3.2.127`                                                                                   |
| `server2`     | `10.3.3.0`       | `255.255.255.240` |            14               | `10.3.3.14`        | `10.3.3.15`                                                                                   |

🌞 **Vous pouvez d'ores-et-déjà créer le routeur. Pour celui-ci, vous me prouverez que :**

- il a bien une IP dans les 3 réseaux, l'IP que vous avez choisie comme IP de passerelle
```
[clement@Router ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:e5:13:ee brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 85704sec preferred_lft 85704sec
    inet6 fe80::a00:27ff:fee5:13ee/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:6d:77:7d brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.62/26 brd 10.3.1.63 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe6d:777d/64 scope link
       valid_lft forever preferred_lft forever
4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:1a:5a:93 brd ff:ff:ff:ff:ff:ff
    inet 10.3.2.126/25 brd 10.3.2.127 scope global noprefixroute enp0s9
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe1a:5a93/64 scope link
       valid_lft forever preferred_lft forever
5: enp0s10: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:9e:1e:4a brd ff:ff:ff:ff:ff:ff
    inet 10.3.3.14/28 brd 10.3.3.15 scope global noprefixroute enp0s10
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe9e:1e4a/64 scope link
       valid_lft forever preferred_lft forever
```

- il a un accès internet
```
[clement@Router ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=54.6 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=61.3 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=114 time=20.3 ms
^C
--- 8.8.8.8 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2005ms
rtt min/avg/max/mdev = 20.250/45.374/61.255/17.971 ms
```

- il a de la résolution de noms
```
[clement@Router ~]$ dig

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>>
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 28694
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 13, AUTHORITY: 0, ADDITIONAL: 27

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;.                              IN      NS

;; ANSWER SECTION:
.                       517256  IN      NS      a.root-servers.net.
.                       517256  IN      NS      b.root-servers.net.
.                       517256  IN      NS      c.root-servers.net.
.                       517256  IN      NS      d.root-servers.net.
.                       517256  IN      NS      e.root-servers.net.
.                       517256  IN      NS      f.root-servers.net.
.                       517256  IN      NS      g.root-servers.net.
.                       517256  IN      NS      h.root-servers.net.
.                       517256  IN      NS      i.root-servers.net.
.                       517256  IN      NS      j.root-servers.net.
.                       517256  IN      NS      k.root-servers.net.
.                       517256  IN      NS      l.root-servers.net.
.                       517256  IN      NS      m.root-servers.net.

;; ADDITIONAL SECTION:
a.root-servers.net.     517256  IN      A       198.41.0.4
a.root-servers.net.     517256  IN      AAAA    2001:503:ba3e::2:30
b.root-servers.net.     517256  IN      A       199.9.14.201
b.root-servers.net.     517256  IN      AAAA    2001:500:200::b
c.root-servers.net.     517256  IN      A       192.33.4.12
c.root-servers.net.     517256  IN      AAAA    2001:500:2::c
d.root-servers.net.     517256  IN      A       199.7.91.13
d.root-servers.net.     517256  IN      AAAA    2001:500:2d::d
e.root-servers.net.     517256  IN      A       192.203.230.10
e.root-servers.net.     517256  IN      AAAA    2001:500:a8::e
f.root-servers.net.     517256  IN      A       192.5.5.241
f.root-servers.net.     517256  IN      AAAA    2001:500:2f::f
g.root-servers.net.     517256  IN      A       192.112.36.4
g.root-servers.net.     517256  IN      AAAA    2001:500:12::d0d
h.root-servers.net.     517256  IN      A       198.97.190.53
h.root-servers.net.     517256  IN      AAAA    2001:500:1::53
i.root-servers.net.     517256  IN      A       192.36.148.17
i.root-servers.net.     517256  IN      AAAA    2001:7fe::53
j.root-servers.net.     517256  IN      A       192.58.128.30
j.root-servers.net.     517256  IN      AAAA    2001:503:c27::2:30
k.root-servers.net.     517256  IN      A       193.0.14.129
k.root-servers.net.     517256  IN      AAAA    2001:7fd::1
l.root-servers.net.     517256  IN      A       199.7.83.42
l.root-servers.net.     517256  IN      AAAA    2001:500:9f::42
m.root-servers.net.     517256  IN      A       202.12.27.33
m.root-servers.net.     517256  IN      AAAA    2001:dc3::35

;; Query time: 64 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Mon Sep 27 11:38:49 CEST 2021
;; MSG SIZE  rcvd: 811
```
Le serveur DNs attribué est 1.1.1.1

- il porte le nom `router.tp3`*
```
[clement@Router ~]$ hostname
Router.tp3
```

- n'oubliez pas [d'activer le routage sur la machine](../../cours/memo/rocky_network.md#activation-du-routage)
```
[clement@Router ~]$ sudo firewall-cmd --list-all
[sudo] password for clement:
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s10 enp0s3 enp0s8 enp0s9
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[clement@Router ~]$ sudo firewall-cmd --get-active-zone
public
  interfaces: enp0s3 enp0s8 enp0s9 enp0s10

[clement@Router ~]$ sudo firewall-cmd --add-masquerade --zone=public
success

[clement@Router ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
success
```

🌞 **Vous remplirez aussi** au fur et à mesure que vous avancez dans le TP, au fur et à mesure que vous créez des machines, **le 🗃️ tableau d'adressage 🗃️ suivant :**

| Nom machine         | Adresse IP `client1` | Adresse IP `server1` | Adresse IP `server2` | Adresse de passerelle |
|---------------------|----------------------|----------------------|----------------------|-----------------------|
| `router.tp3`        | `10.3.1.62/26`       | `10.3.2.126/25`      | `10.3.3.14/28`       | Carte NAT             |
| `dhcp.client1.tp3`  | `10.3.1.11/26`       |        ...           |        ...           | `10.3.1.62/26`        |
|`marcel.client1.tp3` | `10.3.1.12/26`       |        ...           |        ...           | `10.3.1.62/26`        |
| `dns.server1.tp3`   |        ...           | `10.3.2.11/25`       |        ...           | `10.3.2.126/25`       |
| `johny.client1.tp3` | `10.3.1.14/26`       |        ...           |        ...           | `10.3.1.62/26`        |
| `web1.server2.tp3`  |        ...           |        ...           | `10.3.3.11`          | `10.3.3.14`           |
| `nfs1.server2.tp3`  |        ...           |        ...           | `10.3.3.12`          | `10.3.3.14`           |

> *N'oubliez pas de **TOUJOURS** fournir le masque quand vous écrivez une IP.*

# II. Services d'infra

Ce qu'on appelle un "service d'infra" c'est un service qui n'est pas directement utile pour un utilisateur du réseau. Ce n'est pas un truc que l'utilisateur va consommer sciemment, pour obtenir quelque chose.  

Un service d'infra, c'est un service nécessaire pour que l'infra tienne debout, dans des conditions normales de fonctionnement. On trouve, entre autres :

- serveurs DHCP
- serveurs DNS
- serveurs de sauvegarde
- serveurs d'annuaires (Active Directory par exemple)
- serveurs de monitoring (surveillance)

## 1. Serveur DHCP

You already did this in TP 2. Repeat.

🌞 **Mettre en place une machine qui fera office de serveur DHCP** dans le réseau `client1`. Elle devra :

- porter le nom `dhcp.client1.tp3`
- [📝**checklist**📝](#0-prérequis)
- donner une IP aux machines clients qui le demande
- leur donner l'adresse de leur passerelle
- leur donner l'adresse d'un DNS utilisable

📁 **Fichier `dhcpd.conf`**

---

🌞 **Mettre en place un client dans le réseau `client1`**

- de son p'tit nom `marcel.client1.tp3`
- [📝**checklist**📝](#0-prérequis)
- la machine récupérera une IP dynamiquement grâce au serveur DHCP
- ainsi que sa passerelle et une adresse d'un DNS utilisable


🌞 **Depuis `marcel.client1.tp3`**

- prouver qu'il a un accès internet + résolution de noms, avec des infos récupérées par votre DHCP
```
[clement@marcel network-scripts]$ sudo nano ifcfg-enp0s8
[sudo] password for clement:

[clement@marcel network-scripts]$ sudo nmcli con reload

[clement@marcel network-scripts]$ sudo nmcli con up enp0s8
Connection successfully activated (D-Bus active path: /org/freedesktop/NetworkManager/ActiveConnection/2)
[clement@marcel network-scripts]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:26:91:5b brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.12/26 brd 10.3.1.63 scope global dynamic noprefixroute enp0s8
       valid_lft 596sec preferred_lft 596sec
    inet6 fe80::a00:27ff:fe26:915b/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```
Marcel à bien récup une adresse ip depuis le dhcp.

```
[clement@marcel network-scripts]$ dig

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>>
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 46104
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 13, AUTHORITY: 0, ADDITIONAL: 27

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;.                              IN      NS

;; ANSWER SECTION:
.                       516274  IN      NS      a.root-servers.net.
.                       516274  IN      NS      b.root-servers.net.
.                       516274  IN      NS      c.root-servers.net.
.                       516274  IN      NS      d.root-servers.net.
.                       516274  IN      NS      e.root-servers.net.
.                       516274  IN      NS      f.root-servers.net.
.                       516274  IN      NS      g.root-servers.net.
.                       516274  IN      NS      h.root-servers.net.
.                       516274  IN      NS      i.root-servers.net.
.                       516274  IN      NS      j.root-servers.net.
.                       516274  IN      NS      k.root-servers.net.
.                       516274  IN      NS      l.root-servers.net.
.                       516274  IN      NS      m.root-servers.net.

;; ADDITIONAL SECTION:
a.root-servers.net.     516274  IN      A       198.41.0.4
a.root-servers.net.     516274  IN      AAAA    2001:503:ba3e::2:30
b.root-servers.net.     516274  IN      A       199.9.14.201
b.root-servers.net.     516274  IN      AAAA    2001:500:200::b
c.root-servers.net.     516274  IN      A       192.33.4.12
c.root-servers.net.     516274  IN      AAAA    2001:500:2::c
d.root-servers.net.     516274  IN      A       199.7.91.13
d.root-servers.net.     516274  IN      AAAA    2001:500:2d::d
e.root-servers.net.     516274  IN      A       192.203.230.10
e.root-servers.net.     516274  IN      AAAA    2001:500:a8::e
f.root-servers.net.     516274  IN      A       192.5.5.241
f.root-servers.net.     516274  IN      AAAA    2001:500:2f::f
g.root-servers.net.     516274  IN      A       192.112.36.4
g.root-servers.net.     516274  IN      AAAA    2001:500:12::d0d
h.root-servers.net.     516274  IN      A       198.97.190.53
h.root-servers.net.     516274  IN      AAAA    2001:500:1::53
i.root-servers.net.     516274  IN      A       192.36.148.17
i.root-servers.net.     516274  IN      AAAA    2001:7fe::53
j.root-servers.net.     516274  IN      A       192.58.128.30
j.root-servers.net.     516274  IN      AAAA    2001:503:c27::2:30
k.root-servers.net.     516274  IN      A       193.0.14.129
k.root-servers.net.     516274  IN      AAAA    2001:7fd::1
l.root-servers.net.     516274  IN      A       199.7.83.42
l.root-servers.net.     516274  IN      AAAA    2001:500:9f::42
m.root-servers.net.     516274  IN      A       202.12.27.33
m.root-servers.net.     516274  IN      AAAA    2001:dc3::35

;; Query time: 14 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Mon Sep 27 17:03:50 CEST 2021
;; MSG SIZE  rcvd: 811
```
```
[clement@marcel network-scripts]$ ping ynov.com -c2
PING ynov.com (92.243.16.143) 56(84) bytes of data.
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=1 ttl=55 time=12.1 ms
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=2 ttl=55 time=15.0 ms

--- ynov.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1000ms
rtt min/avg/max/mdev = 12.081/13.548/15.016/1.472 ms
```
Marcel à bien également récupérer un adresse de dns : 1.1.1.1 et sait résoudre des noms de domaines.

```
[clement@marcel network-scripts]$ ping 8.8.8.8 -c2
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=117 time=12.4 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=117 time=12.4 ms

--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 12.388/12.391/12.395/0.111 ms
```
Marcel à bien un accès à internet


- à l'aide de la commande `traceroute`, prouver que `marcel.client1.tp3` passe par `router.tp3` pour sortir de son réseau
```
[clement@marcel network-scripts]$ traceroute google.com
traceroute to google.com (216.58.214.78), 30 hops max, 60 byte packets
 1  _gateway (10.3.1.62)  0.894 ms  0.907 ms  0.898 ms
 2  10.0.2.2 (10.0.2.2)  0.886 ms  0.855 ms  0.770 ms
 3  10.0.2.2 (10.0.2.2)  3.086 ms  3.056 ms  2.987 ms
```
Marcel passe par sa gateway afin d'aller sur google.
Or sa gateway correspond bien à celle de notre routeur : **10.3.1.62**

## 2. Serveur DNS

### A. Our own DNS server

Le principe, c'est qu'une fois ce DNS en place, vous n'aurez plus besoin des fichiers `/etc/hosts` sur toutes les machines.  
**Le DNS agit comme un répertoire central, il connaît les noms et IP de tout le monde.**  
Une fois qu'il est là, toutes les machines peuvent lui demander à quelle IP se trouve tel ou tel nom.

On dira dans un premier temps que votre serveur DNS est un serveur DNS ***resolver*** pour votre domaine. C'est à dire qu'il va activement résoudre des noms vers des IP (forward) ou des IP vers des noms (reverse), si on lui pose des questions.  
**Il sera *resolver* pour les zones `server1.tp3`  `server2.tp3` et `client1.tp3`.**

**On dit aussi que votre serveur est ***autoritatif*** pour les zones `server1.tp3`  `server2.tp3` et `client1.tp3`** car c'est lui qui a autorité sur ces zone. C'est lui le serveur qui **SAIT** à quelle IP correspond quel nom.

Dans un deuxième temps, vous lui donnerez aussi, en plus de la fonctionnalité de *resolver* la fonctionnalité de ***forwarder***.  
Il sera alors capable, s'il ne connaît pas la réponse à une requête DNS, de la faire passer à un autre serveur DNS.  

L'idée du *forwarder* ici c'est clairement de lui passer `8.8.8.8` ou `1.1.1.1` en adresse vers qui forward. Comme ça, il répond aux questions pour nos zones en `.tp3` et pour tout le reste, il demande à des DNS publics d'internet.  
**Conséquence** : nos clients pourront résoudre des noms locaux comme `marcel.client1.tp3` mais aussi des noms publics comme `google.com`.

A la fin donc, votre serveur sera :

- autoritatif pour les zones `server1.tp3`  `server2.tp3` et `client1.tp3`
- resolver local
- forwarder (pour résoudre les noms publics)

**C BO NON ?!** 🔥🔥🔥🔥

### B. SETUP copain

> Nous avons dans ce TP 3 zones : `client1.tp3`, `server1.tp3`, `server2.tp3`. J'avoue j'suis pas gentil, ça fait beaucoup de fichiers direct. Ca fait manipuler :D

🌞 **Mettre en place une machine qui fera office de serveur DNS**

- dans le réseau `server1`
- de son p'tit nom `dns1.server1.tp3`
- [📝**checklist**📝](#0-prérequis)
- il faudra lui ajouter un serveur DNS public connu, afin qu'il soit capable de résoudre des noms publics comme `google.com`
- comme pour le DHCP, on part sur "rocky linux dns server" on Google pour les détails de l'install
  - le paquet que vous allez installer devrait s'appeler `bind` : c'est le nom du serveur DNS le plus utilisé au monde
- il y aura plusieurs fichiers de conf :
  - un fichier de conf principal
  - des fichiers de zone "forward"
    - permet d'indiquer une correspondance nom -> IP
  - des fichiers de zone "reverse"
    - permet d'indiquer une correspondance IP -> nom
  - on ne met **PAS** les clients dans les fichiers de zone car leurs adresses IP peuvent changer (ils les récupèrent à l'aide du DHCP)
```
[clement@dns ~]$ sudo nano /etc/named.conf

[clement@dns ~]$ cat /etc/resolv.conf
# Generated by NetworkManager
search server1.tp3
nameserver 1.1.1.1
```

> 💡 Je vous recommande de d'abord mettre en place une simple zone *Forward* et de tester. Puis la zone *Reverse* associée, puis de tester. Et enfin, d'ajouter les autres zones. Procédez de façon itérative, pour comprendre d'où viennent vos erreurs quand elles surviennent.

🌞 **Tester le DNS depuis `marcel.client1.tp3`**

- définissez **manuellement** l'utilisation de votre serveur DNS
- essayez une résolution de nom avec `dig`
  - une résolution de nom classique
    - `dig <NOM>` pour obtenir l'IP associée à un nom
    - on teste la zone forward
  - une résolution inverse
    - `dig -x <IP>` pour obtenir le nom associé à une IP
    - on teste le zone reverse
- prouvez que c'est bien votre serveur DNS qui répond pour chaque `dig`

```
[clement@marcel ~]$ dig dns.server1.tp3
*
; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> dns.server1.tp3
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 51426
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 087b75a8432ec637dd072aba615c16bc734048e09fc04e2f (good)
;; QUESTION SECTION:
;dns.server1.tp3.               IN      A

;; ANSWER SECTION:
dns.server1.tp3.        259200  IN      A       10.3.2.11

;; AUTHORITY SECTION:
server1.tp3.            259200  IN      NS      dns.server1.tp3.

;; Query time: 1 msec
;; SERVER: 10.3.2.11#53(10.3.2.11)
;; WHEN: Tue Oct 05 11:11:29 CEST 2021
;; MSG SIZE  rcvd: 102



```

Par exemple, vous devriez pouvoir exécuter les commandes suivantes :

```bash
$ dig marcel.client1.tp3
$ dig dhcp.client1.tp3
```

> Pour rappel, avec `dig`on peut préciser directement sur la ligne de commande à quel serveur poser la question avec  le caractère `@`. Par exemple `dig google.com @8.8.8.8` permet de préciser explicitement qu'on veut demander à `8.8.8.8` à quelle IP se trouve `google.com`.

⚠️ **NOTE : A partir de maintenant, vous devrez modifier les fichiers de zone pour chaque nouvelle machine ajoutée (voir [📝**checklist**📝](#0-prérequis).**

🌞 Configurez l'utilisation du serveur DNS sur TOUS vos noeuds

- les serveurs, on le fait à la main
- les clients, c'est fait *via* DHCP

⚠️**A partir de maintenant, vous n'utiliserez plus DU TOUT les IPs pour communiquer entre vos machines, mais uniquement leurs noms.**

## 3. Get deeper

On va affiner un peu la configuration des outils mis en place.

### A. DNS forwarder

🌞 **Affiner la configuration du DNS**

- faites en sorte que votre DNS soit désormais aussi un forwarder DNS
- c'est à dire que s'il ne connaît pas un nom, il ira poser la question à quelqu'un d'autre

> Hint : c'est la clause `recursion` dans le fichier `/etc/named.conf`. Et c'est déjà activé par défaut en fait.

🌞 Test !

- vérifier depuis `marcel.client1.tp3` que vous pouvez résoudre des noms publics comme `google.com` en utilisant votre propre serveur DNS (commande `dig`)
- pour que ça fonctionne, il faut que `dns1.server1.tp3` soit lui-même capable de résoudre des noms, avec `1.1.1.1` par exemple
```
[clement@marcel ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 4439
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 4, ADDITIONAL: 9

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 60f2732dafda94b1c3904779615c1bd2fe31d50043a15653 (good)
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             300     IN      A       216.58.214.174

;; AUTHORITY SECTION:
google.com.             172800  IN      NS      ns4.google.com.
google.com.             172800  IN      NS      ns1.google.com.
google.com.             172800  IN      NS      ns2.google.com.
google.com.             172800  IN      NS      ns3.google.com.

;; ADDITIONAL SECTION:
ns2.google.com.         172800  IN      A       216.239.34.10
ns1.google.com.         172800  IN      A       216.239.32.10
ns3.google.com.         172800  IN      A       216.239.36.10
ns4.google.com.         172800  IN      A       216.239.38.10
ns2.google.com.         172800  IN      AAAA    2001:4860:4802:34::a
ns1.google.com.         172800  IN      AAAA    2001:4860:4802:32::a
ns3.google.com.         172800  IN      AAAA    2001:4860:4802:36::a
ns4.google.com.         172800  IN      AAAA    2001:4860:4802:38::a

;; Query time: 231 msec
;; SERVER: 10.3.2.11#53(10.3.2.11)
;; WHEN: Tue Oct 05 11:33:12 CEST 2021
;; MSG SIZE  rcvd: 331
```


### B. On revient sur la conf du DHCP

🌞 **Affiner la configuration du DHCP**

- faites en sorte que votre DHCP donne désormais l'adresse de votre serveur DNS aux clients
- créer un nouveau client `johnny.client1.tp3` qui récupère son IP, et toutes les nouvelles infos, en DHCP

```
[clement@johny ~]$ sudo nmcli con show enp0s8 | grep -i dhcp
[sudo] password for clement:
ipv4.dhcp-client-id:                    --
ipv4.dhcp-iaid:                         --
ipv4.dhcp-timeout:                      0 (default)
ipv4.dhcp-send-hostname:                yes
ipv4.dhcp-hostname:                     --
ipv4.dhcp-fqdn:                         --
ipv4.dhcp-hostname-flags:               0x0 (none)
ipv4.dhcp-vendor-class-identifier:      --
ipv4.dhcp-reject-servers:               --
ipv6.dhcp-duid:                         --
ipv6.dhcp-iaid:                         --
ipv6.dhcp-timeout:                      0 (default)
ipv6.dhcp-send-hostname:                yes
ipv6.dhcp-hostname:                     --
ipv6.dhcp-hostname-flags:               0x0 (none)
DHCP4.OPTION[1]:                        broadcast_address = 10.3.1.63
DHCP4.OPTION[2]:                        dhcp_lease_time = 600
DHCP4.OPTION[3]:                        dhcp_server_identifier = 10.3.1.11
DHCP4.OPTION[4]:                        domain_name_servers = 10.3.2.11
DHCP4.OPTION[5]:                        expiry = 1633429173
DHCP4.OPTION[6]:                        ip_address = 10.3.1.14
DHCP4.OPTION[7]:                        requested_broadcast_address = 1
DHCP4.OPTION[8]:                        requested_domain_name = 1
DHCP4.OPTION[9]:                        requested_domain_name_servers = 1
DHCP4.OPTION[10]:                       requested_domain_search = 1
DHCP4.OPTION[11]:                       requested_host_name = 1
DHCP4.OPTION[12]:                       requested_interface_mtu = 1
DHCP4.OPTION[13]:                       requested_ms_classless_static_routes = 1
DHCP4.OPTION[14]:                       requested_nis_domain = 1
DHCP4.OPTION[15]:                       requested_nis_servers = 1
DHCP4.OPTION[16]:                       requested_ntp_servers = 1
DHCP4.OPTION[17]:                       requested_rfc3442_classless_static_routes = 1
DHCP4.OPTION[18]:                       requested_root_path = 1
DHCP4.OPTION[19]:                       requested_routers = 1
DHCP4.OPTION[20]:                       requested_static_routes = 1
DHCP4.OPTION[21]:                       requested_subnet_mask = 1
DHCP4.OPTION[22]:                       requested_time_offset = 1
DHCP4.OPTION[23]:                       requested_wpad = 1
DHCP4.OPTION[24]:                       routers = 10.3.1.62
DHCP4.OPTION[25]:                       subnet_mask = 255.255.255.192
```

---

# Entracte

**YO !**

**On se pose deux minutes pour apprécier le travail réalisé.**

A ce stade vous avez :

- un routeur qui permet aux machines d'acheminer leur trafic entre les réseaux
  - entre les LANs
  - vers internet
- un DHCP qui filent des infos à vos clients
  - une IP, une route par défaut, l'adresse d'un DNS
- un DNS
  - qui résout tous les noms localement

Vous le sentez là que ça commence à prendre forme oupa ? Tous les réseaux du monde sont fichus comme ça c:

**Allez, prend un cookie tu l'as mérité : 🍪**

![You get a cookie](./pic/you-get-a-cookie-ricky-berwick.gif "You get a cookie")

---

# III. Services métier

Ce qu'on appelle un "service métier", à l'inverse du service d'infra, c'est un truc que l'utilisateur final veut consommer. On le dit "métier" car dans une entreprise, c'est ce service qui sert le métier de l'entreprise, son coeur d'activité.

Par exemple, pour une agence de développement web, l'un des services métier, bah c'est les serveurs web qui sont déployés pour porter les sites web développés par la boîte.

> On est en 2021, le serveur web c'est le cas d'école. Y'en a partout. Mais genre + que vous imaginez, même nous les admins, on utilise de plus en plus d'interfaces web, au détriment de la ligne de commande.

## 1. Serveur Web

🌞 **Setup d'une nouvelle machine, qui sera un serveur Web, une belle appli pour nos clients**

- réseau `server2`
- hello `web1.server2.tp3` !
- [📝**checklist**📝](#0-prérequis)
- vous utiliserez le serveur web que vous voudrez, le but c'est d'avoir un serveur web fast, pas d'y passer 1000 ans :)
  - réutilisez [votre serveur Web du TP1 Linux](https://gitlab.com/it4lik/b2-linux-2021/-/tree/main/tp/1#2-cr%C3%A9ation-de-service)
  - ou montez un bête NGINX avec la page d'accueil (ça se limite à un `dnf install` puis `systemctl start nginx`)
  - ou ce que vous voulez, du moment que c'est fast
  - **dans tous les cas, n'oubliez pas d'ouvrir le port associé dans le firewall** pour que le serveur web soit joignable

> Une bête page HTML fera l'affaire. On est pas là pour faire du design. Et vous prenez pas la tête pour l'install, appelez-moi vite s'il faut, on est pas en système ni en Linux non plus. On est en réseau, on veut juste un serveur web derrière un port :)
```
[clement@web1 ~]$ curl localhost:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
[...]
</html>
```
🌞 **Test test test et re-test**  

- testez que votre serveur web est accessible depuis `marcel.client1.tp3`
  - utilisez la commande `curl` pour effectuer des requêtes HTTP
```
[clement@marcel ~]$ curl 10.3.3.11:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
[...]
</body>
</html>
```

---

👋👋👋 **HEY ! C'est beau là.** On a un client qui consomme un serveur web, avec toutes les infos récupérées en DHCP, DNS, blablabla + un routeur maison.  

Je sais que ça se voit pas trop avec les `curl`. Vous pourriez installer un Ubuntu graphique sur le client si vous voulez vous y croire à fond, avec un ptit Firefox, Google Chrome ou whatever.  

**Mais là on y est**, vous avez un ptit réseau, un vrai, avec tout ce qu'il faut.

## 2. Partage de fichiers

### A. L'introduction wola

Dans cette partie, on va monter un serveur NFS. C'est un serveur qui servira à partager des fichiers à travers le réseau.  

**En d'autres termes, certaines machines pourront accéder à un dossier, à travers le réseau.**

Dans notre cas, on va faire en sorte que notre serveur web `web1.server2.tp3` accède à un partage de fichier.

> Dans un cas réel, ce partage de fichiers peut héberger le site web servi par notre serveur Web par exemple.  
Ou, de manière plus récurrente, notre serveur Web peut effectuer des sauvegardes dans ce dossier. Ainsi, le serveur de partage de fichiers devient le serveur qui centralise les sauvegardes.

### B. Le setup wola

🌞 **Setup d'une nouvelle machine, qui sera un serveur NFS**

- réseau `server2`
- bienvenue dans la partie `nfs1.server2.tp3` !
- [📝**checklist**📝](#0-prérequis)
- je crois que vous commencez à connaître la chanson... Google "nfs server rocky linux"
  - [ce lien me semble être particulièrement simple et concis](https://www.server-world.info/en/note?os=Rocky_Linux_8&p=nfs&f=1)
- **vous partagerez un dossier créé à cet effet : `/srv/nfs_share/`**
```
[clement@nfs1 ~]$ systemctl enable --now rpcbind nfs-server
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-unit-files ====
Authentication is required to manage system service or unit files.
Authenticating as: clement
Password:
==== AUTHENTICATION COMPLETE ====
==== AUTHENTICATING FOR org.freedesktop.systemd1.reload-daemon ====
Authentication is required to reload the systemd state.
Authenticating as: clement
Password:
==== AUTHENTICATION COMPLETE ====
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ====
Authentication is required to start 'rpcbind.service'.
Authenticating as: clement
Password:
==== AUTHENTICATION COMPLETE ====
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ====
Authentication is required to start 'nfs-server.service'.
Authenticating as: clement
Password:
==== AUTHENTICATION COMPLETE ====
```

🌞 **Configuration du client NFS**

- effectuez de la configuration sur `web1.server2.tp3` pour qu'il accède au partage NFS
- le partage NFS devra être monté dans `/srv/nfs/`
- [sur le même site, y'a ça](https://www.server-world.info/en/note?os=Rocky_Linux_8&p=nfs&f=2)
```
[clement@web1 ~]$ sudo mount -t nfs 10.3.3.12:/home/nfsshare /mnt
[clement@web1 ~]$ df -hT
10.3.3.12:/home/nfsshare nfs4      6.2G  2.2G  4.1G  35% /mnt
```

🌞 **TEEEEST**

- tester que vous pouvez lire et écrire dans le dossier `/srv/nfs` depuis `web1.server2.tp3`
Création de fichier depuis le client nfs
```
[clement@web1 mnt]$ sudo touch toto.txt
[sudo] password for clement:
[clement@web1 mnt]$ ls
toto.txt
```

Vérifions que le fichier toto apparait bien sur le server nfs
```
[clement@nfs1 nfsshare]$ ls
toto.txt
```

Création de fichier depuis le nfs avec du contenu random
```
[clement@nfs1 nfsshare]$ sudo nano litMoi.txt
[clement@nfs1 nfsshare]$ cat litMoi.txt
Well done !!
```

Lisons le fichier depuis le client nfs 
```
[clement@web1 mnt]$ ls
litMoi.txt  toto.txt
[clement@web1 mnt]$ cat litMoi.txt
Well done !!
```

- vous devriez voir les modifications du côté de  `nfs1.server2.tp3` dans le dossier `/srv/nfs_share/`

# IV. Un peu de théorie : TCP et UDP

Bon bah avec tous ces services, on a de la matière pour bosser sur TCP et UDP. P'tite partie technique pure avant de conclure.

🌞 **Déterminer, pour chacun de ces protocoles, s'ils sont encapsulés dans du TCP ou de l'UDP :**

- SSH
- HTTP
- DNS
- NFS

📁 **Captures réseau `tp3_ssh.pcap`, `tp3_http.pcap`, `tp3_dns.pcap` et `tp3_nfs.pcap`**

> **Prenez le temps** de réfléchir à pourquoi on a utilisé TCP ou UDP pour transmettre tel ou tel protocole. Réfléchissez à quoi servent chacun de ces protocoles, et de ce qu'on a besoin qu'ils réalisent.

🌞 **Expliquez-moi pourquoi je ne pose pas la question pour DHCP.**

🌞 **Capturez et mettez en évidence un *3-way handshake***

📁 **Capture réseau `tp3_3way.pcap`**

# V. El final

🌞 **Bah j'veux un schéma.**

- réalisé avec l'outil de votre choix
- un schéma clair qui représente
  - les réseaux
    - les adresses de réseau devront être visibles
  - toutes les machines, avec leurs noms
  - devront figurer les IPs de toutes les interfaces réseau du schéma
  - pour les serveurs : une indication de quel port est ouvert
- vous représenterez les host-only comme des switches
- dans le rendu, mettez moi ici à la fin :
  - le schéma
  - le 🗃️ tableau des réseaux 🗃️
  - le 🗃️ tableau d'adressage 🗃️
    - on appelle ça aussi un "plan d'adressage IP" :)

> J'vous le dis direct, un schéma moche avec Paint c'est -5 Points. Je vous recommande [draw.io](http://draw.io).

🌞 **Et j'veux des fichiers aussi, tous les fichiers de conf du DNS**

- 📁 Fichiers de zone
- 📁 Fichier de conf principal DNS `named.conf`
- faites ça à peu près propre dans le rendu, que j'ai plus qu'à cliquer pour arriver sur le fichier ce serait top




PS : Je me suis trompé sur les attributions d'ip. Ayant déjà avancé sur le Tp, j'ai donc continué avec les mêmes ips.
Voici les tableaux demandés avec les bonnes addresses :

| Nom du réseau | Adresse du réseau | Masque            | Nombre de clients possibles | Adresse passerelle | [Adresse broadcast](../../cours/lexique/README.md#adresse-de-diffusion-ou-broadcast-address) |
|---------------|-------------------|-------------------|-----------------------------|--------------------|----------------------------------------------------------------------------------------------|
| `client1`     | `10.3.1.128`      | `255.255.255.192` |            62               | `10.3.1.190`        | `10.3.1.191`                                                                                   |
| `server1`     | `10.3.1.0`        | `255.255.255.128` |            126              | `10.3.1.126`       | `10.3.1.127`                                                                                  |
| `server2`     | `10.3.1.192`      | `255.255.255.240` |            14               | `10.3.1.206`        | `10.3.1.207`                                                                                   |


 Nom machine         | Adresse IP `client1` | Adresse IP `server1` | Adresse IP `server2`  | Adresse de passerelle |
|---------------------|----------------------|----------------------|----------------------|-----------------------|
| `router.tp3`        | `10.3.1.190/26`      | `10.3.1.126/28`      | `10.3.1.206/28`      | Carte NAT             |
| `dhcp.client1.tp3`  | `10.3.1.129/26`      |       ...            |       ...            | `10.3.1.190/26`       |
|`marcel.client1.tp3` | `10.3.1.130/26`      |       ...            |       ...            | `10.3.1.190/26`       |
| `dns.server1.tp3`   |        ...           | `10.3.1.1/28`        |       ...            | `10.3.1.126/28`       |
| `johny.client1.tp3` | `10.3.1.131/26`      |        ...           |        ...           | `10.3.1.190/26`       |
| `web1.server2.tp3`  |        ...           |        ...           | `10.3.1.193`          | `10.3.1.206/28`       |
| `nfs1.server2.tp3`  |        ...           |        ...           | `10.3.1.194`          | `10.3.1.206/28`       |

